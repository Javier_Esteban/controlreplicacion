﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace ControlReplicacionV
{

    public class ClienteIp
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        // ManualResetEvent instances signal completion.  
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        // The response from the remote device.  
        private static String response = String.Empty;

        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }


        public static bool enviarComando(Comando cmd)
        {
            bool envioCorrecto = false;
            
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // The name of the   
                // remote device is "host.contoso.com".  
                IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
                // IPAddress ipAddress = ipHostInfo.AddressList[0];


                String dirIp = null;
                if ((bool)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_esServidorPrincipal])
                    dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];


                if ((bool)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_esServidorSecundario])
                    dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorPrincipal];

                if (dirIp != null)
                {

                    int port = Int32.Parse((String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_puertoServidorSecundario]);

                    IPAddress ipAddress = IPAddress.Parse(dirIp);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                    // Create a TCP/IP socket.  
                    //int contador = 0;
                    try
                    {
                        Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        //client.ReceiveTimeout = 5000;
                        // Connect to the remote endpoint.  

                        //solicitamos conexión
                        client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                        //esperamos a que la conexión se haya realizado
                        bool res = connectDone.WaitOne(1000);
                        if (res)
                        {
                            //incluimos un retardo para evitar excepción de socket
                            Thread.Sleep(500);

                            XmlSerializer serializer = new XmlSerializer(typeof(Comando));

                            StringWriter sw = new StringWriter();
                            //MemoryStream ms = new MemoryStream();
                            serializer.Serialize(sw, cmd);
                            //ms.GetBuffer();
                            string comandoS = sw.ToString();

                            Send(client, comandoS);

                            // Send test data to the remote device.  
                            //Send(client, "This is a test<EOF>");
                            res = sendDone.WaitOne(2000);
                            if (!res)
                                log.Error("Error en envio de comando, NO se ha realizado el envio en el tiempo especificado");
                            else
                                envioCorrecto = true;

                        }
                        else
                        {
                            log.Error("No se puede realizar la conexión, comando no enviado.");
                        }

                        // Release the socket.  
                        client.Shutdown(SocketShutdown.Both);
                        client.Close();
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e.ToString());
                        log.Error("Error en envio de comando", e);

                    }

                }
                else
                    log.Error("Abandonada ejecución de cliente, no se corresponde ip servidor local ni remoto.");

            }
            catch (Exception e)
            {
                log.Error("Error en envio de comando", e);
                //Console.WriteLine(e.ToString());
            }

            return envioCorrecto;
        }

        public static void StartClient()
        {
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // The name of the   
                // remote device is "host.contoso.com".  
                IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
                // IPAddress ipAddress = ipHostInfo.AddressList[0];


                String  dirIp = null;
                if((bool)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_esServidorPrincipal])
                    dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];


                if ((bool)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_esServidorSecundario])
                    dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorPrincipal];

                if (dirIp != null)
                {

                    int port = Int32.Parse((String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_puertoServidorSecundario]);

                    IPAddress ipAddress = IPAddress.Parse(dirIp);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);


                    // Create a TCP/IP socket.  

                    //int contador = 0;

                    try
                    {
                        Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        //client.ReceiveTimeout = 5000;
                        // Connect to the remote endpoint.  

                        //solicitamos conexión
                        client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                        //esperamos a que la conexión se haya realizado
                        bool res = connectDone.WaitOne(1000);
                        if (res)
                        {
                            do
                            {
                                ComandoEstadoServicios comando = new ComandoEstadoServicios("ON", "OFF");
                                comando.estadoServidor = "Off";
                                comando.estadoConcentrador = "On";

                                XmlSerializer serializer = new XmlSerializer(typeof(Comando));

                                StringWriter sw = new StringWriter();
                                //MemoryStream ms = new MemoryStream();
                                serializer.Serialize(sw, comando);
                                //ms.GetBuffer();
                                string comandoS = sw.ToString();


                                Send(client, comandoS);


                                // Send test data to the remote device.  
                                //Send(client, "This is a test<EOF>");
                                sendDone.WaitOne();

                                Thread.Sleep(1000);

                            } while (true);
                        }


                        //// Receive the response from the remote device.  
                        //Receive(client);
                        //receiveDone.WaitOne();

                        //// Write the response to the console.  
                        ////Console.WriteLine("Response received : {0}", response);
                        //log.Debug("Recibida respuesta." + response);

                        //Thread.Sleep(1000);

                        // Release the socket.  
                        client.Shutdown(SocketShutdown.Both);
                        client.Close();
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e.ToString());
                        log.Error("Error en envio de comando", e);

                    }

                }
                else
                    log.Error("Abandonada ejecución de cliente, no se corresponde ip servidor local ni remoto.");

            }
            catch (Exception e)
            {
                log.Error("Error en envio de comando", e);
                //Console.WriteLine(e.ToString());
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                //Console.WriteLine("Socket connected to {0}",client.RemoteEndPoint.ToString());
                log.Debug("Socket conectado a " + client.RemoteEndPoint.ToString());


                // Signal that the connection has been made.  
                connectDone.Set();
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
                log.Error("Error en connnect call back.",e);

            }
        }

        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = client;
                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                log.Error("Error en recepcion.", e);
                //Console.WriteLine(e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    // Get the rest of the data.  
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                log.Error("Error en recepcion call back", e);

                //Console.WriteLine(e.ToString());
            }
        }

        private static void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to server.", bytesSent);
                log.Debug("Enviados " + bytesSent + " caracteres");

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}

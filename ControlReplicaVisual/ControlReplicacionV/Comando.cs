﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ControlReplicacionV
{

    
    [XmlInclude(typeof(ComandoCambioIP))]
    [XmlInclude(typeof(ComandoEstadoServicios))]
    [XmlInclude(typeof(ComandoPararArquero))]
    [XmlInclude(typeof(ComandoLanzarArquero))]
    [XmlInclude(typeof(ComandoLatido))]
    public class Comando
    {
        public const int ID_COMANDO_LATIDO = 0;
        public const int ID_COMANDO_LANZAR_ARQUERO = 1;
        public const int ID_COMANDO_PARAR_ARQUERO = 2;
        public const int ID_ESTADO_SERVICIOS = 3;
        public const int ID_CAMBIO_IP = 4;

        //public enum COMANDOEN{ ID_COMANDO_LATIDO = 0, ID_COMANDO_LANZAR_ARQUERO = 1}

        protected int idComando;
        protected string _fecha;
        protected string _fechaRecibido;
        protected string parametro1;
        protected string parametro2;
        protected string parametro3;
        protected int parametro4;
        protected int parametro5;

        public Comando() {
            //yyyy - MM - ddTHH:mm: sszzz
            _fecha = System.DateTime.Now.ToString("yyyyMMddHHmmss");
        }


        //public Comando(int id)
        //{
        //    this.idComando = id;
        //}

        public Comando(int id,String param1)
        {
            this.idComando = id;
            this.parametro1 = param1;
        }

        public int id
        {
            get => idComando;
            set => idComando = value;
        }
        public string fecha
        {
            get => _fecha;
            set => _fecha = value;
        }

        public string fechaRecibido
        {
            get => _fechaRecibido;
            set => _fechaRecibido = value;
        }

        public string param1
        {
            get => parametro1;
            set => parametro1 = value;
        }
        public string param2
        {
            get => parametro2;
            set => parametro2 = value;
        }
        public string param3
        {
            get => parametro3;
            set => parametro3 = value;
        }
        public int param4
        {
            get => parametro4;
            set => parametro4 = value;
        }
        public int param5
        {
            get => parametro5;
            set => parametro5 = value;
        }
        
    }

    public class ComandoLatido:Comando
    {
        public ComandoLatido()
        {
            idComando = ID_COMANDO_LATIDO;
        }
    }

    public class ComandoLanzarArquero : Comando
    {
        public ComandoLanzarArquero()
        {
            idComando = ID_COMANDO_LANZAR_ARQUERO;
        }
    }

    public class ComandoPararArquero : Comando
    {
        public ComandoPararArquero()
        {
            idComando = ID_COMANDO_PARAR_ARQUERO;
        }
    }

    public class ComandoEstadoServicios:Comando
    {
        protected string _estadoServidor;
        protected string _estadoConcentrador;
        public const string EQUIPO_INI = "--";
        public const string EQUIPO_ON = "COMUNICA";
        public const string EQUIPO_OFF = "NO COMUNICA";


        public const string SERVICIO_ON = "LANZADO";
        public const string SERVICIO_OFF = "PARADO";
        public const string SERVICIO_INI = "--";


        public ComandoEstadoServicios()
        {
            idComando = ID_ESTADO_SERVICIOS;
        }

        public ComandoEstadoServicios(string estadoServidor,string estadoConcentrador)
        {
             
            idComando = ID_ESTADO_SERVICIOS;
            this._estadoServidor = estadoServidor;
            this._estadoConcentrador = estadoConcentrador;
        }

        public string estadoServidor
        {
            get => _estadoServidor;
            set => _estadoServidor = value;
        }

        public string estadoConcentrador
        {
            get => _estadoConcentrador;
            set => _estadoConcentrador = value;
        }
    }

    public class ComandoCambioIP : Comando
    {
        protected String _nuevaIp;

        public ComandoCambioIP()
        {
            idComando = ID_CAMBIO_IP;
        }

        public ComandoCambioIP(string nuevaIp)
        {

            idComando = ID_CAMBIO_IP;
            this._nuevaIp = nuevaIp;
        }

        public string nuevaIp
        {
            get => _nuevaIp;
            set => _nuevaIp = value;
        }
    }


}

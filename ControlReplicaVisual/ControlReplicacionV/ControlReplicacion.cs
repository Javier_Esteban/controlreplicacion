﻿using System;
using System.ServiceProcess;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace ControlReplicacionV
{
    public class ControlReplicacion
    {

        FrmMain formppal;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string ipArquero = "";
        public static Object block = new Object();
        public const int LANZAR_SERVICIO = 0;
        public const int PARAR_SERVICIO = 1;

        private static ControlReplicacion instance;

        public static ControlReplicacion Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ControlReplicacion();
                }
                return instance;
            }
        }


        public void operarServicio(String nombreServicio,int operacion,string texto)
        {

            try
            {
                //lanzamos servicio
                ServiceController[] servicios = ServiceController.GetServices();
                foreach (ServiceController servicio in servicios)
                {
                    if (servicio.ServiceName.Equals(nombreServicio))
                    {
                        switch (operacion)
                        {
                            case LANZAR_SERVICIO:
                                if (servicio.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                                {
                                    log.Info("Lanzamos el servicio <" + servicio.ServiceName + ">");
                                    servicio.Start();
                                }
                                else
                                {
                                    log.Info("No se lanza el servicio<" + servicio.ServiceName + ">.No esta parado.");
                                }
                                formppal.SetText(formppal.lEstadoEjecucionAuxiliar, texto);

                                break;
                            case PARAR_SERVICIO:
                                if (servicio.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                                {
                                    log.Info("Paramos el servicio <" + servicio.ServiceName + ">");
                                    servicio.Stop();
                                }
                                else
                                {
                                    log.Info("No se para el servicio<" + servicio.ServiceName + ">.Ya está parado.");
                                }
                                break;
                        }

                    }
                }

            }
            catch (Exception e)
            {
                log.Error("Error en lanzamiento del servicio<" + nombreServicio + ">",e);
            }

        }

        /**
         * Devuelve un comando con los estados de los servicios que encuentre entre los servicios existentes
         * 
         */
        public ComandoEstadoServicios dameEstadoServicios()
        {
            ComandoEstadoServicios res = new ComandoEstadoServicios();
            try
            {
                string nombreConcentrador = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador];

                string nombreServidor = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreServidor];

                ServiceController[] servicios = ServiceController.GetServices();
                foreach (ServiceController servicio in servicios)
                {
                    if (servicio.ServiceName.Equals(nombreConcentrador) || servicio.ServiceName.Equals(nombreServidor))
                    {
                        string estado = null;
                        switch (servicio.Status)
                        {
                            case System.ServiceProcess.ServiceControllerStatus.Stopped:
                                estado = ComandoEstadoServicios.SERVICIO_OFF;
                                break;
                            case System.ServiceProcess.ServiceControllerStatus.Running:
                                estado = ComandoEstadoServicios.SERVICIO_ON;
                                break;

                        }
                        if (servicio.ServiceName.Equals(nombreConcentrador))
                            res.estadoConcentrador = estado;
                        else
                            res.estadoServidor = estado;
                    }
                }



            }
            catch (Exception e)
            {
                log.Error("Error en obtención del estado de los servicios.", e);
            }
            return res;
        }

        public void StartProcess(Object form)
        {

            //esperamos a que se carge el formulario antes de empezar a pintar.
            //Thread.Sleep(5000);

            formppal = (FrmMain)form;

            do
            {

                try
                {
                    ServiceReference1.EstadoServicios estadoPrincipal = GestorParametrosIniciales.Instance.getServidorConexion().dame_estado_servicios((int)Servidor.RolEquipo.PRINCIPAL);
                    GestorParametrosIniciales.Instance.servidorPrincipal.setEstados(estadoPrincipal);
                    presentaTextoEstados((FrmMain)form, estadoPrincipal, (int)Servidor.RolEquipo.PRINCIPAL);
                    


                    ServiceReference1.EstadoServicios estadoSecundario = GestorParametrosIniciales.Instance.getServidorConexion().dame_estado_servicios((int)Servidor.RolEquipo.SECUNDARIO);
                    GestorParametrosIniciales.Instance.servidorSecundario.setEstados(estadoSecundario);
                    presentaTextoEstados((FrmMain)form, estadoSecundario, (int)Servidor.RolEquipo.SECUNDARIO);

                    //ServiceReference1.EstadoServicios estadoAuxiliar = GestorParametrosIniciales.Instance.getServidorConexion().dame_estado_servicios((int)Servidor.RolEquipo.AUXILIAR);
                    //presentaTextoEstados((FrmMain)form, estadoAuxiliar, (int)Servidor.RolEquipo.AUXILIAR);


                    string titulo = "Servidor - " + GestorParametrosIniciales.Instance.getServidorConexion().dirIP;
                    ((FrmMain)form).SetText(((FrmMain)form).lServidorActual, titulo);

                    string hora = DateTime.Now.ToString("HH:mm:ss");
                    ((FrmMain)form).SetText(((FrmMain)form).lReloj, hora);

                    Thread.Sleep(5000);

                }
                catch (Exception e)
                {
                    log.Error("Error no controlado en ciclo principal.", e);
                }
            } while (true);

        }

        private static void presentaTextoEstados(FrmMain form, ServiceReference1.EstadoServicios estado,int rol)
        {
            Control labelEstadoEquipo = null;
            Control labelControlador = null;
            //Control[] labelServicios = null;
            //Control[] labelServidor = null;
            Control labelEstadoEjecucion = null;


            switch (rol)
            {
                case ((int)Servidor.RolEquipo.PRINCIPAL):
                    if(estado!=null) form.SetText(form.gEquipoPrincipal, "PRINCIPAL - " + estado.dirIP);
                    labelEstadoEquipo = form.lEstadoEquipoPrincipal;
                    labelControlador = form.lEstadoControladorPrincipal;
                    //labelServicios = form.lEstadoServicioLocal2;
                    //labelServidor = form.lEstadoServicioLocal1;
                    labelEstadoEjecucion = form.lEstadoEjecucionPrincipal;
                    break;
                case ((int)Servidor.RolEquipo.SECUNDARIO):
                    if (estado != null) form.SetText(form.gEquipoSecundario, "SECUNDARIO - " + estado.dirIP);
                    labelEstadoEquipo = form.lEstadoEquipoSecundario;
                    labelControlador = form.lEstadoControladorSecundario;
                    //labelServicios = form.lEstadoServicioRemoto2;
                    //labelServidor = form.lEstadoServicioRemoto1;
                    labelEstadoEjecucion = form.lEstadoEjecucionSecundario;
                    break;
                //case ((int)Servidor.RolEquipo.AUXILIAR):
                //    if (estado != null) form.SetText(form.gEquipoAuxiliar, "AUXILIAR - " + estado.dirIP);
                //    labelEstadoEquipo = form.lEstadoEquipoAuxiliar;
                //    labelControlador = form.lEstadoControladorAuxiliar;
                //    labelEstadoEjecucion = form.lEstadoEjecucionAuxiliar;
                //    break;
            }

            if (estado != null)
            {

                switch (estado.estadoEquipo)
                {
                    case (int)Servidor.EstadoEquipo.COMUNICA:
                        form.SetText(labelEstadoEquipo, ComandoEstadoServicios.EQUIPO_ON);
                        break;
                    case (int)Servidor.EstadoEquipo.SIN_COMUNICACIONES:
                        form.SetText(labelEstadoEquipo, ComandoEstadoServicios.EQUIPO_OFF, Color.Red);
                        break;
                    case (int)Servidor.EstadoEquipo.INICIAL:
                        form.SetText(labelEstadoEquipo, ComandoEstadoServicios.EQUIPO_INI);
                        break;
                }

                switch (estado.estadoControlador)
                {
                    case (int)Servidor.EstadoEquipo.COMUNICA:
                        form.SetText(labelControlador, ComandoEstadoServicios.EQUIPO_ON);
                        break;
                    case (int)Servidor.EstadoEquipo.SIN_COMUNICACIONES:
                        form.SetText(labelControlador, ComandoEstadoServicios.EQUIPO_OFF,Color.Red);
                        break;
                    case (int)Servidor.EstadoEquipo.INICIAL:
                        form.SetText(labelControlador, ComandoEstadoServicios.EQUIPO_INI);
                        break;
                }

                Dictionary<string, int> todosServicios = new Dictionary<string, int>();
                foreach (var dict in estado.estadoServicios) todosServicios.Add(dict.Key, dict.Value);
                foreach (var dict in estado.estadoOtrosServicios) todosServicios.Add(dict.Key, dict.Value);

                int indice = 1;
                foreach (var servicio in todosServicios)
                {
                    //int estadoServicio = estado.estadoServicios[servicio.Key];
                    Label labelServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_NOMBRE_SERVICIO, indice);
                    if(labelServicio!=null)
                        form.SetText(labelServicio, servicio.Key);
                    Label labelEstadoServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_ESTADO_SERVICIO, indice);
                    if (labelEstadoServicio != null)
                    {
                        switch (servicio.Value)
                        {
                            case (int)Servidor.EstadoServicio.LANZADO:
                                form.SetText(labelEstadoServicio, ComandoEstadoServicios.SERVICIO_ON);
                                break;
                            case (int)Servidor.EstadoServicio.PARADO:
                                form.SetText(labelEstadoServicio, ComandoEstadoServicios.SERVICIO_OFF, Color.Red);
                                break;
                            case (int)Servidor.EstadoServicio.INICIAL:
                            case (int)Servidor.EstadoServicio.SIN_COMUNICACION:
                                form.SetText(labelEstadoServicio, ComandoEstadoServicios.SERVICIO_INI);
                                break;
                        }
                    }
                    indice++;
                }
                for (int i = indice; i <= 6; i++)
                {
                    Label labelServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_NOMBRE_SERVICIO, i);
                    if (labelServicio!=null)
                        form.SetText(labelServicio, "");
                    labelServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_ESTADO_SERVICIO, i);
                    if (labelServicio != null)
                        form.SetText(labelServicio, "");
                }



                //if (labelConcentrador != null)
                //{
                //    switch (estado.estadoConcentrador)
                //    {
                //    case (int)Servidor.EstadoServicio.LANZADO:
                //        form.SetText(labelConcentrador, ComandoEstadoServicios.SERVICIO_ON);
                //        break;
                //    case (int)Servidor.EstadoServicio.PARADO:
                //        form.SetText(labelConcentrador, ComandoEstadoServicios.SERVICIO_OFF, Color.Red);
                //        break;
                //    case (int)Servidor.EstadoServicio.INICIAL:
                //    case (int)Servidor.EstadoServicio.SIN_COMUNICACION:
                //        form.SetText(labelConcentrador, ComandoEstadoServicios.SERVICIO_INI);
                //        break;
                //    }
                //}

                //if (labelServidor != null)
                //{
                //    switch (estado.estadoServidor)
                //    {
                //        case (int)Servidor.EstadoServicio.LANZADO:
                //            form.SetText(labelServidor, ComandoEstadoServicios.SERVICIO_ON);
                //            break;
                //        case (int)Servidor.EstadoServicio.PARADO:
                //            form.SetText(labelServidor, ComandoEstadoServicios.SERVICIO_OFF, Color.Red);
                //            break;
                //        case (int)Servidor.EstadoServicio.INICIAL:
                //        case (int)Servidor.EstadoServicio.SIN_COMUNICACION:
                //            form.SetText(labelServidor, ComandoEstadoServicios.SERVICIO_INI);
                //            break;
                //    }
                //}


                switch (estado.rolEjecucion)
                {
                    case (int)Servidor.RolEjecucion.NORMAL:
                        switch (rol)
                        {
                            case ((int)Servidor.RolEquipo.PRINCIPAL):
                                form.SetText(labelEstadoEjecucion, "EQUIPO PRINCIPAL.", Color.Aqua);
                                break;
                            case ((int)Servidor.RolEquipo.SECUNDARIO):
                                form.SetText(labelEstadoEjecucion, "EQUIPO SECUNDARIO.", Color.Aqua);
                                break;
                        }
                        break;
                    case (int)Servidor.RolEjecucion.CONTROL_CEDIDO:
                        form.SetText(labelEstadoEjecucion, "PRINCIPAL HA CEDIDO EL CONTROL.", Color.Red);
                        break;

                    case (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO:
                        form.SetText(labelEstadoEjecucion, "SECUNDARIO HA TOMADO EL CONTROL.", Color.Red);
                        break;
                    case (int)Servidor.RolEjecucion.ERROR_CONFIGURACION:
                        form.SetText(labelEstadoEjecucion, "ERROR DE CONFIGURACIÓN.", Color.Red);
                        break;
                }

            }
            else
            {
                form.SetText(labelEstadoEquipo, ComandoEstadoServicios.EQUIPO_OFF, Color.Red);
                form.SetText(labelControlador, ComandoEstadoServicios.EQUIPO_OFF, Color.Red);
                for (int i = 1; i <= 6; i++)
                {
                    Label labelServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_NOMBRE_SERVICIO, i);
                    if (labelServicio != null)
                        form.SetText(labelServicio, "");
                    labelServicio = form.dameEtiqueta(rol, FrmMain.ETIQUETA_ESTADO_SERVICIO, i);
                    if (labelServicio != null)
                        form.SetText(labelServicio, "");
                }

                //if(labelServicios!=null)
                //    form.SetText(labelServicios, ComandoEstadoServicios.SERVICIO_INI);
                //if(labelServidor!=null)
                //form.SetText(labelServidor, ComandoEstadoServicios.SERVICIO_INI);

            }

        }

        public static bool CambiarIP(string ipActual,string ipNueva)
        {
            bool res = true;

            string mascaraRed = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_mascaraRed];
            try
            {
                NetworkManagement netmng = new NetworkManagement();
                netmng.setIP(ipActual, ipNueva, mascaraRed);

            }catch(Exception e)
            {
                log.Error("Error en cambio de ip.", e);
                res = false;
            }

            return res;
        }

        public static bool hacer_ping(string destino)
        {
            bool res = false;

            Ping HacerPing = new Ping();
             int iTiempoEspera = 2000;
            PingReply RespuestaPing;
            RespuestaPing = HacerPing.Send(destino, iTiempoEspera);
            if (RespuestaPing.Status == IPStatus.Success)
            {
                //txtLog.AppendText("Ping a " +sDireccion.ToString() +"[" +RespuestaPing.Address.ToString() +"]" +" Correcto" +" Tiempo de respuesta = " +RespuestaPing.RoundtripTime.ToString() +" ms" +"\n")
                res = true;
            }

            return res;
        }


    }
}
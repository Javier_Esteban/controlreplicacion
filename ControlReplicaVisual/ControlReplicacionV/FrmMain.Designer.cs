﻿namespace ControlReplicacionV
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pServicios = new System.Windows.Forms.Panel();
            this.lEstadoEjecucionAuxiliar = new System.Windows.Forms.Label();
            this.btn_lanzarPrimario = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lReloj = new System.Windows.Forms.Label();
            this.lServidorActual = new System.Windows.Forms.Label();
            this.gEquipoAuxiliar = new System.Windows.Forms.GroupBox();
            this.lEstadoEquipoAuxiliar = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gEquipoPrincipal = new System.Windows.Forms.GroupBox();
            this.lEstadoServicioLocal5 = new System.Windows.Forms.Label();
            this.lEstadoServicioLocal6 = new System.Windows.Forms.Label();
            this.lServicioLocal5 = new System.Windows.Forms.Label();
            this.lServicioLocal6 = new System.Windows.Forms.Label();
            this.lEstadoServicioLocal3 = new System.Windows.Forms.Label();
            this.lEstadoServicioLocal4 = new System.Windows.Forms.Label();
            this.lServicioLocal3 = new System.Windows.Forms.Label();
            this.lServicioLocal4 = new System.Windows.Forms.Label();
            this.lEstadoEjecucionPrincipal = new System.Windows.Forms.Label();
            this.lEstadoEquipoPrincipal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lEstadoControladorPrincipal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lEstadoServicioLocal1 = new System.Windows.Forms.Label();
            this.lEstadoServicioLocal2 = new System.Windows.Forms.Label();
            this.lServicioLocal1 = new System.Windows.Forms.Label();
            this.lServicioLocal2 = new System.Windows.Forms.Label();
            this.btn_lanzarSecundario = new System.Windows.Forms.Button();
            this.gEquipoSecundario = new System.Windows.Forms.GroupBox();
            this.lEstadoServicioRemoto5 = new System.Windows.Forms.Label();
            this.lServicioRemoto5 = new System.Windows.Forms.Label();
            this.lEstadoServicioRemoto6 = new System.Windows.Forms.Label();
            this.lServicioRemoto6 = new System.Windows.Forms.Label();
            this.lEstadoServicioRemoto3 = new System.Windows.Forms.Label();
            this.lServicioRemoto3 = new System.Windows.Forms.Label();
            this.lEstadoServicioRemoto4 = new System.Windows.Forms.Label();
            this.lServicioRemoto4 = new System.Windows.Forms.Label();
            this.lEstadoEjecucionSecundario = new System.Windows.Forms.Label();
            this.lEstadoControladorSecundario = new System.Windows.Forms.Label();
            this.lControladorRemoto = new System.Windows.Forms.Label();
            this.lEstadoEquipoSecundario = new System.Windows.Forms.Label();
            this.lEstadoRemoto = new System.Windows.Forms.Label();
            this.lEstadoServicioRemoto1 = new System.Windows.Forms.Label();
            this.lServicioRemoto1 = new System.Windows.Forms.Label();
            this.lEstadoServicioRemoto2 = new System.Windows.Forms.Label();
            this.lServicioRemoto2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pServicios.SuspendLayout();
            this.gEquipoAuxiliar.SuspendLayout();
            this.gEquipoPrincipal.SuspendLayout();
            this.gEquipoSecundario.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            resources.ApplyResources(this.notifyIcon1, "notifyIcon1");
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pServicios);
            this.tabPage1.Controls.Add(this.menuStrip1);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pServicios
            // 
            this.pServicios.Controls.Add(this.lEstadoEjecucionAuxiliar);
            this.pServicios.Controls.Add(this.btn_lanzarPrimario);
            this.pServicios.Controls.Add(this.label3);
            this.pServicios.Controls.Add(this.label1);
            this.pServicios.Controls.Add(this.lReloj);
            this.pServicios.Controls.Add(this.lServidorActual);
            this.pServicios.Controls.Add(this.gEquipoAuxiliar);
            this.pServicios.Controls.Add(this.gEquipoPrincipal);
            this.pServicios.Controls.Add(this.btn_lanzarSecundario);
            this.pServicios.Controls.Add(this.gEquipoSecundario);
            resources.ApplyResources(this.pServicios, "pServicios");
            this.pServicios.Name = "pServicios";
            // 
            // lEstadoEjecucionAuxiliar
            // 
            resources.ApplyResources(this.lEstadoEjecucionAuxiliar, "lEstadoEjecucionAuxiliar");
            this.lEstadoEjecucionAuxiliar.BackColor = System.Drawing.Color.Aqua;
            this.lEstadoEjecucionAuxiliar.Name = "lEstadoEjecucionAuxiliar";
            // 
            // btn_lanzarPrimario
            // 
            this.btn_lanzarPrimario.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.btn_lanzarPrimario, "btn_lanzarPrimario");
            this.btn_lanzarPrimario.Name = "btn_lanzarPrimario";
            this.btn_lanzarPrimario.UseVisualStyleBackColor = true;
            this.btn_lanzarPrimario.Click += new System.EventHandler(this.btn_lanzarPrimario_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lReloj
            // 
            resources.ApplyResources(this.lReloj, "lReloj");
            this.lReloj.Name = "lReloj";
            // 
            // lServidorActual
            // 
            resources.ApplyResources(this.lServidorActual, "lServidorActual");
            this.lServidorActual.Name = "lServidorActual";
            this.lServidorActual.Click += new System.EventHandler(this.lServidorActual_Click);
            // 
            // gEquipoAuxiliar
            // 
            this.gEquipoAuxiliar.Controls.Add(this.lEstadoEquipoAuxiliar);
            this.gEquipoAuxiliar.Controls.Add(this.label4);
            resources.ApplyResources(this.gEquipoAuxiliar, "gEquipoAuxiliar");
            this.gEquipoAuxiliar.Name = "gEquipoAuxiliar";
            this.gEquipoAuxiliar.TabStop = false;
            // 
            // lEstadoEquipoAuxiliar
            // 
            resources.ApplyResources(this.lEstadoEquipoAuxiliar, "lEstadoEquipoAuxiliar");
            this.lEstadoEquipoAuxiliar.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoEquipoAuxiliar.ForeColor = System.Drawing.Color.Black;
            this.lEstadoEquipoAuxiliar.Name = "lEstadoEquipoAuxiliar";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // gEquipoPrincipal
            // 
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal5);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal6);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal5);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal6);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal3);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal4);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal3);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal4);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoEjecucionPrincipal);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoEquipoPrincipal);
            this.gEquipoPrincipal.Controls.Add(this.label8);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoControladorPrincipal);
            this.gEquipoPrincipal.Controls.Add(this.label6);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal1);
            this.gEquipoPrincipal.Controls.Add(this.lEstadoServicioLocal2);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal1);
            this.gEquipoPrincipal.Controls.Add(this.lServicioLocal2);
            resources.ApplyResources(this.gEquipoPrincipal, "gEquipoPrincipal");
            this.gEquipoPrincipal.Name = "gEquipoPrincipal";
            this.gEquipoPrincipal.TabStop = false;
            // 
            // lEstadoServicioLocal5
            // 
            resources.ApplyResources(this.lEstadoServicioLocal5, "lEstadoServicioLocal5");
            this.lEstadoServicioLocal5.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal5.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal5.Name = "lEstadoServicioLocal5";
            // 
            // lEstadoServicioLocal6
            // 
            resources.ApplyResources(this.lEstadoServicioLocal6, "lEstadoServicioLocal6");
            this.lEstadoServicioLocal6.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal6.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal6.Name = "lEstadoServicioLocal6";
            // 
            // lServicioLocal5
            // 
            resources.ApplyResources(this.lServicioLocal5, "lServicioLocal5");
            this.lServicioLocal5.Name = "lServicioLocal5";
            // 
            // lServicioLocal6
            // 
            resources.ApplyResources(this.lServicioLocal6, "lServicioLocal6");
            this.lServicioLocal6.Name = "lServicioLocal6";
            // 
            // lEstadoServicioLocal3
            // 
            resources.ApplyResources(this.lEstadoServicioLocal3, "lEstadoServicioLocal3");
            this.lEstadoServicioLocal3.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal3.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal3.Name = "lEstadoServicioLocal3";
            // 
            // lEstadoServicioLocal4
            // 
            resources.ApplyResources(this.lEstadoServicioLocal4, "lEstadoServicioLocal4");
            this.lEstadoServicioLocal4.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal4.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal4.Name = "lEstadoServicioLocal4";
            // 
            // lServicioLocal3
            // 
            resources.ApplyResources(this.lServicioLocal3, "lServicioLocal3");
            this.lServicioLocal3.Name = "lServicioLocal3";
            // 
            // lServicioLocal4
            // 
            resources.ApplyResources(this.lServicioLocal4, "lServicioLocal4");
            this.lServicioLocal4.Name = "lServicioLocal4";
            // 
            // lEstadoEjecucionPrincipal
            // 
            resources.ApplyResources(this.lEstadoEjecucionPrincipal, "lEstadoEjecucionPrincipal");
            this.lEstadoEjecucionPrincipal.BackColor = System.Drawing.Color.Aqua;
            this.lEstadoEjecucionPrincipal.Name = "lEstadoEjecucionPrincipal";
            // 
            // lEstadoEquipoPrincipal
            // 
            resources.ApplyResources(this.lEstadoEquipoPrincipal, "lEstadoEquipoPrincipal");
            this.lEstadoEquipoPrincipal.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoEquipoPrincipal.ForeColor = System.Drawing.Color.Black;
            this.lEstadoEquipoPrincipal.Name = "lEstadoEquipoPrincipal";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lEstadoControladorPrincipal
            // 
            resources.ApplyResources(this.lEstadoControladorPrincipal, "lEstadoControladorPrincipal");
            this.lEstadoControladorPrincipal.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoControladorPrincipal.ForeColor = System.Drawing.Color.Black;
            this.lEstadoControladorPrincipal.Name = "lEstadoControladorPrincipal";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // lEstadoServicioLocal1
            // 
            resources.ApplyResources(this.lEstadoServicioLocal1, "lEstadoServicioLocal1");
            this.lEstadoServicioLocal1.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal1.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal1.Name = "lEstadoServicioLocal1";
            // 
            // lEstadoServicioLocal2
            // 
            resources.ApplyResources(this.lEstadoServicioLocal2, "lEstadoServicioLocal2");
            this.lEstadoServicioLocal2.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioLocal2.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioLocal2.Name = "lEstadoServicioLocal2";
            // 
            // lServicioLocal1
            // 
            resources.ApplyResources(this.lServicioLocal1, "lServicioLocal1");
            this.lServicioLocal1.Name = "lServicioLocal1";
            // 
            // lServicioLocal2
            // 
            resources.ApplyResources(this.lServicioLocal2, "lServicioLocal2");
            this.lServicioLocal2.Name = "lServicioLocal2";
            this.lServicioLocal2.Click += new System.EventHandler(this.lConcentradorLocal_Click);
            // 
            // btn_lanzarSecundario
            // 
            this.btn_lanzarSecundario.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.btn_lanzarSecundario, "btn_lanzarSecundario");
            this.btn_lanzarSecundario.Name = "btn_lanzarSecundario";
            this.btn_lanzarSecundario.UseVisualStyleBackColor = true;
            this.btn_lanzarSecundario.Click += new System.EventHandler(this.btn_lanzarSecundario_Click);
            // 
            // gEquipoSecundario
            // 
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto5);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto5);
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto6);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto6);
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto3);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto3);
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto4);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto4);
            this.gEquipoSecundario.Controls.Add(this.lEstadoEjecucionSecundario);
            this.gEquipoSecundario.Controls.Add(this.lEstadoControladorSecundario);
            this.gEquipoSecundario.Controls.Add(this.lControladorRemoto);
            this.gEquipoSecundario.Controls.Add(this.lEstadoEquipoSecundario);
            this.gEquipoSecundario.Controls.Add(this.lEstadoRemoto);
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto1);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto1);
            this.gEquipoSecundario.Controls.Add(this.lEstadoServicioRemoto2);
            this.gEquipoSecundario.Controls.Add(this.lServicioRemoto2);
            resources.ApplyResources(this.gEquipoSecundario, "gEquipoSecundario");
            this.gEquipoSecundario.Name = "gEquipoSecundario";
            this.gEquipoSecundario.TabStop = false;
            // 
            // lEstadoServicioRemoto5
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto5, "lEstadoServicioRemoto5");
            this.lEstadoServicioRemoto5.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto5.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto5.Name = "lEstadoServicioRemoto5";
            // 
            // lServicioRemoto5
            // 
            resources.ApplyResources(this.lServicioRemoto5, "lServicioRemoto5");
            this.lServicioRemoto5.Name = "lServicioRemoto5";
            // 
            // lEstadoServicioRemoto6
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto6, "lEstadoServicioRemoto6");
            this.lEstadoServicioRemoto6.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto6.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto6.Name = "lEstadoServicioRemoto6";
            // 
            // lServicioRemoto6
            // 
            resources.ApplyResources(this.lServicioRemoto6, "lServicioRemoto6");
            this.lServicioRemoto6.Name = "lServicioRemoto6";
            // 
            // lEstadoServicioRemoto3
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto3, "lEstadoServicioRemoto3");
            this.lEstadoServicioRemoto3.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto3.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto3.Name = "lEstadoServicioRemoto3";
            // 
            // lServicioRemoto3
            // 
            resources.ApplyResources(this.lServicioRemoto3, "lServicioRemoto3");
            this.lServicioRemoto3.Name = "lServicioRemoto3";
            // 
            // lEstadoServicioRemoto4
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto4, "lEstadoServicioRemoto4");
            this.lEstadoServicioRemoto4.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto4.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto4.Name = "lEstadoServicioRemoto4";
            // 
            // lServicioRemoto4
            // 
            resources.ApplyResources(this.lServicioRemoto4, "lServicioRemoto4");
            this.lServicioRemoto4.Name = "lServicioRemoto4";
            // 
            // lEstadoEjecucionSecundario
            // 
            resources.ApplyResources(this.lEstadoEjecucionSecundario, "lEstadoEjecucionSecundario");
            this.lEstadoEjecucionSecundario.BackColor = System.Drawing.Color.Aqua;
            this.lEstadoEjecucionSecundario.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lEstadoEjecucionSecundario.Name = "lEstadoEjecucionSecundario";
            // 
            // lEstadoControladorSecundario
            // 
            resources.ApplyResources(this.lEstadoControladorSecundario, "lEstadoControladorSecundario");
            this.lEstadoControladorSecundario.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoControladorSecundario.ForeColor = System.Drawing.Color.Black;
            this.lEstadoControladorSecundario.Name = "lEstadoControladorSecundario";
            // 
            // lControladorRemoto
            // 
            resources.ApplyResources(this.lControladorRemoto, "lControladorRemoto");
            this.lControladorRemoto.Name = "lControladorRemoto";
            // 
            // lEstadoEquipoSecundario
            // 
            resources.ApplyResources(this.lEstadoEquipoSecundario, "lEstadoEquipoSecundario");
            this.lEstadoEquipoSecundario.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoEquipoSecundario.ForeColor = System.Drawing.Color.Black;
            this.lEstadoEquipoSecundario.Name = "lEstadoEquipoSecundario";
            // 
            // lEstadoRemoto
            // 
            resources.ApplyResources(this.lEstadoRemoto, "lEstadoRemoto");
            this.lEstadoRemoto.Name = "lEstadoRemoto";
            // 
            // lEstadoServicioRemoto1
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto1, "lEstadoServicioRemoto1");
            this.lEstadoServicioRemoto1.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto1.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto1.Name = "lEstadoServicioRemoto1";
            // 
            // lServicioRemoto1
            // 
            resources.ApplyResources(this.lServicioRemoto1, "lServicioRemoto1");
            this.lServicioRemoto1.Name = "lServicioRemoto1";
            // 
            // lEstadoServicioRemoto2
            // 
            resources.ApplyResources(this.lEstadoServicioRemoto2, "lEstadoServicioRemoto2");
            this.lEstadoServicioRemoto2.BackColor = System.Drawing.Color.Transparent;
            this.lEstadoServicioRemoto2.ForeColor = System.Drawing.Color.Black;
            this.lEstadoServicioRemoto2.Name = "lEstadoServicioRemoto2";
            // 
            // lServicioRemoto2
            // 
            resources.ApplyResources(this.lServicioRemoto2, "lServicioRemoto2");
            this.lServicioRemoto2.Name = "lServicioRemoto2";
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listBox1);
            this.tabPage2.Controls.Add(this.label5);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.MultiColumn = true;
            this.listBox1.Name = "listBox1";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem,
            this.acercaDeToolStripMenuItem});
            resources.ApplyResources(this.menuStrip2, "menuStrip2");
            this.menuStrip2.Name = "menuStrip2";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            resources.ApplyResources(this.salirToolStripMenuItem, "salirToolStripMenuItem");
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            resources.ApplyResources(this.acercaDeToolStripMenuItem, "acercaDeToolStripMenuItem");
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            // 
            // FrmMain
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Resize += new System.EventHandler(this.FrmMain_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.pServicios.ResumeLayout(false);
            this.pServicios.PerformLayout();
            this.gEquipoAuxiliar.ResumeLayout(false);
            this.gEquipoAuxiliar.PerformLayout();
            this.gEquipoPrincipal.ResumeLayout(false);
            this.gEquipoPrincipal.PerformLayout();
            this.gEquipoSecundario.ResumeLayout(false);
            this.gEquipoSecundario.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.GroupBox gEquipoPrincipal;
        private System.Windows.Forms.Button btn_lanzarSecundario;
        public System.Windows.Forms.GroupBox gEquipoSecundario;
        public System.Windows.Forms.Label lEstadoEquipoSecundario;
        private System.Windows.Forms.Label lEstadoRemoto;
        public System.Windows.Forms.Label lEstadoServicioRemoto1;
        public System.Windows.Forms.Label lServicioRemoto1;
        public System.Windows.Forms.Label lEstadoServicioRemoto2;
        public System.Windows.Forms.Label lServicioRemoto2;
        public System.Windows.Forms.Label lEstadoServicioLocal1;
        public System.Windows.Forms.Label lEstadoServicioLocal2;
        public System.Windows.Forms.Label lServicioLocal1;
        public System.Windows.Forms.Label lServicioLocal2;
        private System.Windows.Forms.Panel pServicios;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        public System.Windows.Forms.Label lEstadoControladorSecundario;
        public System.Windows.Forms.Label lControladorRemoto;
        public System.Windows.Forms.GroupBox gEquipoAuxiliar;
        public System.Windows.Forms.Label lEstadoEquipoAuxiliar;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lEstadoControladorPrincipal;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lEstadoEquipoPrincipal;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lEstadoEjecucionPrincipal;
        public System.Windows.Forms.Label lEstadoEjecucionSecundario;
        public System.Windows.Forms.Label lEstadoEjecucionAuxiliar;
        public System.Windows.Forms.Label lServidorActual;
        public System.Windows.Forms.Label lReloj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lEstadoServicioLocal5;
        public System.Windows.Forms.Label lEstadoServicioLocal6;
        public System.Windows.Forms.Label lServicioLocal5;
        public System.Windows.Forms.Label lServicioLocal6;
        public System.Windows.Forms.Label lEstadoServicioLocal3;
        public System.Windows.Forms.Label lEstadoServicioLocal4;
        public System.Windows.Forms.Label lServicioLocal3;
        public System.Windows.Forms.Label lServicioLocal4;
        public System.Windows.Forms.Label lEstadoServicioRemoto5;
        public System.Windows.Forms.Label lServicioRemoto5;
        public System.Windows.Forms.Label lEstadoServicioRemoto6;
        public System.Windows.Forms.Label lServicioRemoto6;
        public System.Windows.Forms.Label lEstadoServicioRemoto3;
        public System.Windows.Forms.Label lServicioRemoto3;
        public System.Windows.Forms.Label lEstadoServicioRemoto4;
        public System.Windows.Forms.Label lServicioRemoto4;
        private System.Windows.Forms.Button btn_lanzarPrimario;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static ControlReplicacionV.Servidor;

namespace ControlReplicacionV
{
    // This delegate enables asynchronous calls for setting  
    // the text property on a control.  
    delegate void StringArgReturningVoidDelegate(Control control,string text,Object background);
    delegate void ponPresentacionControTransferidodDelegate(string mensaje);

    public partial class FrmMain : Form
    {
        public static int ETIQUETA_SERVICIO_PRINCIPAL = 0;
        public static int ETIQUETA_SERVICIO_SECUNDARIO = 1;
        public static int ETIQUETA_NOMBRE_SERVICIO = 0;
        public static int ETIQUETA_ESTADO_SERVICIO = 1;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //visualizamos como system try
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Items.AddRange(new object[] {});

            foreach (Control c in gEquipoPrincipal.Controls.OfType<Label>()){
                if(c.Name.StartsWith("lServicioLocal") ||
                    c.Name.StartsWith("lEstadoServicioLocal")) { 
                    c.Text = "";
                }
            }
            foreach (Control c in gEquipoSecundario.Controls.OfType<Label>()){
                if (c.Name.StartsWith("lServicioRemoto") ||
                    c.Name.StartsWith("lEstadoServicioRemoto"))
                {
                    c.Text = "";
                }
            }

            //lServicioLocal1.Text = String.Format(lServicioLocal1.Text, GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreServidor]);
            //lServicioRemoto1.Text = String.Format(lServicioRemoto1.Text, GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreServidor]);
            //string nombreConcentrador = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador];
            //if (nombreConcentrador == null || nombreConcentrador.Trim().Length == 0)
            //{
            //    lServicioLocal2.Visible = false;
            //    lEstadoServicioLocal2.Visible = false;
            //    lServicioRemoto2.Visible = false;
            //    lEstadoServicioRemoto2.Visible = false;
            //}
            //else
            //{
            //    lServicioLocal2.Text = String.Format(lServicioLocal2.Text, GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador]);
            //    lServicioRemoto2.Text = String.Format(lServicioRemoto2.Text, GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador]);
            //}

            //Label c = dameEtiqueta(ETIQUETA_SERVICIO_PRINCIPAL, ETIQUETA_NOMBRE_SERVICIO, 1);
            //c = dameEtiqueta(ETIQUETA_SERVICIO_PRINCIPAL, ETIQUETA_ESTADO_SERVICIO, 1);
            //c= dameEtiqueta(ETIQUETA_SERVICIO_SECUNDARIO, ETIQUETA_NOMBRE_SERVICIO, 1);
            //c= dameEtiqueta(ETIQUETA_SERVICIO_SECUNDARIO, ETIQUETA_ESTADO_SERVICIO, 1);

        }




        //private void btn_lanzarSecundario_Click(object sender, EventArgs e)
        //{

        //    if ((bool)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_esServidorPrincipal])
        //    {
        //        string dirIp = ((IPAddress)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipDireccionLocal]).ToString();
        //        string dirSecundario = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];
        //        string dirAux = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];

        //        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        //        string caption = "Aviso de transferenciá de gestión";
        //        DialogResult result = MessageBox.Show("Desea transferir la gestión de Arquero al equipo "+ dirSecundario + "?.", caption, buttons);
        //        if(result == System.Windows.Forms.DialogResult.Yes)
        //        {
        //            Comando comando = new ComandoLanzarArquero();
        //            //comprobamos antes que no exista ningún equipo con esta ip(responda a pin)
        //            bool res = ControlReplicacion.hacer_ping(dirAux);
        //            if (!res)
        //            {
        //                res = ClienteIp.enviarComando(comando);
        //                if (res)
        //                {
        //                    //cambiamos dirección a dirección auxiliar
        //                    res = ControlReplicacion.CambiarIP(dirIp,dirAux);
        //                    if (res)
        //                    {
        //                        this.ponPresentacionEquipoSecundario("CONTROL TRANSFERIDO A EQUIPO REMOTO");

        //                        buttons = MessageBoxButtons.OK;
        //                        result = MessageBox.Show("Operación realizada con éxito. Nueva direccion:" + dirAux, caption, buttons);

        //                    }
        //                    else
        //                    {
        //                        buttons = MessageBoxButtons.OK;
        //                        result = MessageBox.Show("Error en el cambio de IP.Operación NO realizada.", caption, buttons);
        //                    }
        //                }
        //                else
        //                {
        //                    buttons = MessageBoxButtons.OK;
        //                    result = MessageBox.Show("No se ha podido comunicar con el servidor " + dirAux + ". Se cancela la transferencia.", caption, buttons);
        //                }
        //            }
        //            else
        //            {
        //                buttons = MessageBoxButtons.OK;
        //                result = MessageBox.Show("Ya existe un equipo con la direccion auxiliar " + dirAux + ". Se cancela la transferencia.", caption, buttons);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        MessageBoxButtons buttons = MessageBoxButtons.OK;
        //        string caption = "Aviso de transferenciá de gestión";
        //        DialogResult result = MessageBox.Show("No se puede realizar la transferencia de gestión, no es el servidor principal.", caption, buttons);
        //    }


        //}

        private void FrmMain_Resize(object sender, EventArgs e)
        {
            //notifyIcon1.BalloonTipTitle = "Controlado de replicación";
            //notifyIcon1.BalloonTipText = "Pulse doble click sobre el icono para ver la aplicación de nuevo.";

            //if (FormWindowState.Minimized == this.WindowState)
            //{
            //    notifyIcon1.Visible = true;
            //    //notifyIcon1.ShowBalloonTip(500);
            //    this.Hide();
            //}

            //else if (FormWindowState.Normal == this.WindowState)
            //{
            //    notifyIcon1.Visible = false;
            //}

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        public void SetText(Control control, string text)
        {
            SetText(control, text, null);
        }

        public void SetText(Control control,string text, Object background)
        {
            if (control == null)
                return;
            // InvokeRequired required compares the thread ID of the  
            // calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (control.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(SetText);
                this.Invoke(d, new object[] { control, text ,background});
            }
            else
            {
                if (!control.Text.Equals(text))
                {
                    control.Text = text;
                    if (background != null)
                    {
                        control.BackColor = (Color)background;
                    }
                    else
                    {
                        control.BackColor = Color.Transparent;
                    }
                }
            }
        }
        public void ponPresentacionEquipoSecundario(string mensaje)
        {
            if (pServicios.InvokeRequired)
            {
                ponPresentacionControTransferidodDelegate d = new ponPresentacionControTransferidodDelegate(ponPresentacionEquipoSecundario);
                this.Invoke(d, new object[] {});
            }
            else
            {
                this.pServicios.Enabled = false;
                this.lEstadoEjecucionAuxiliar.Text = mensaje;
                this.lEstadoEjecucionAuxiliar.Visible = true;

            }
        }

        public void AddItemslistParametros(Dictionary<string, Object> items)
        {
            // InvokeRequired required compares the thread ID of the  
            // calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (this.lEstadoServicioRemoto1.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(SetText);
                this.Invoke(d, new object[] { items });
            }
            else
            {
                foreach (KeyValuePair<string, Object> item in items)
                {
                    listBox1.Items.Add(item);
                    //Console.WriteLine(string.Format("Key-{0}:Value-{1}", result.Key, result.Value));
                }


            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
            string caption = "Confirmación de salir.";
            DialogResult result = MessageBox.Show("Seguro que desea abandonar la aplicación.", caption, buttons);
            if (result.Equals(DialogResult.OK))
            {
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    Program.finalizar();
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }

        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 formAcercade = new AboutBox1();
            formAcercade.ShowDialog();
        }

        private void lConcentradorLocal_Click(object sender, EventArgs e)
        {

        }

        public Label dameEtiqueta(int rol,int nombreEstado,int indice)
        {
            Control res = null;

            switch (rol)
            {
                case ((int)Servidor.RolEquipo.PRINCIPAL):
                    if (nombreEstado == ETIQUETA_NOMBRE_SERVICIO)
                    {
                        //foreach (Control c in gEquipoPrincipal.Controls.OfType<Label>())
                        Control[] c = gEquipoPrincipal.Controls.Find("lServicioLocal" + indice, false);
                        if (c.Length > 0)
                        {
                            res = c[0];
                        }
                    }
                    else
                    {
                        Control[] c = gEquipoPrincipal.Controls.Find("lEstadoServicioLocal" + indice, false);
                        if (c.Length > 0)
                        {
                            res = c[0];
                        }

                    }
                    break;
                case ((int)Servidor.RolEquipo.SECUNDARIO):

                    if (nombreEstado == ETIQUETA_NOMBRE_SERVICIO)
                    {
                        //foreach (Control c in gEquipoPrincipal.Controls.OfType<Label>())
                        Control[] c = gEquipoSecundario.Controls.Find("lServicioRemoto" + indice, false);
                        if (c.Length > 0)
                        {
                            res = c[0];
                        }
                    }
                    else
                    {
                        Control[] c = gEquipoSecundario.Controls.Find("lEstadoServicioRemoto" + indice, false);
                        if (c.Length > 0)
                        {
                            res = c[0];
                        }

                    }

                    break;
            }
            return (Label)res;
        }

        private void lServidorActual_Click(object sender, EventArgs e)
        {

        }
        private void btn_lanzarSecundario_Click(object sender, EventArgs e)
        {

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            string caption = "Aviso de transferenciá de gestión";
            DialogResult result = MessageBox.Show("Seguro que desea transferir la gestión de los servicios al equipo Secundario?", caption, buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                if ((GestorParametrosIniciales.Instance.servidorSecundario.estadoEquipo == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorSecundario.estadoControlador == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorPrincipal.estadoControlador == (int)EstadoEquipo.COMUNICA))
                {
                    //Primero lanzamos el principal, para evitar duplicidad de ip.
                    bool res = GestorParametrosIniciales.Instance.servidorPrincipal.transfiere_control();
                    if (res)
                    {
                        res = GestorParametrosIniciales.Instance.servidorSecundario.transfiere_control();
                        if (res)
                        {
                            buttons = MessageBoxButtons.OK;
                            result = MessageBox.Show("Operación realizada con éxito.", caption, buttons);
                        }
                        else
                        {
                            GestorParametrosIniciales.Instance.servidorPrincipal.restaura_control();
                            buttons = MessageBoxButtons.OK;
                            result = MessageBox.Show("Se ha producido un error en la transferencia de control. Revise los logs del equipo secundario para ver las causas.", caption, buttons);
                        }
                    }
                }
                else
                {
                    buttons = MessageBoxButtons.OK;
                    result = MessageBox.Show("Deben encontrarse ambos equipos con comunicaciones. No ha podido realizarse la operación.", caption, buttons);

                }
            }
        }

        private void btn_lanzarPrimario_Click(object sender, EventArgs e)
        {

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            string caption = "Aviso de transferenciá de gestión";
            DialogResult result = MessageBox.Show("Seguro que desea transferir la gestión de los servicios al equipo Principal?", caption, buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                if ((GestorParametrosIniciales.Instance.servidorSecundario.estadoEquipo == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorSecundario.estadoControlador == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)EstadoEquipo.COMUNICA) &&
                    (GestorParametrosIniciales.Instance.servidorPrincipal.estadoControlador == (int)EstadoEquipo.COMUNICA))
                {
                    bool res = GestorParametrosIniciales.Instance.servidorSecundario.restaura_control();
                    if (res)
                    {
                        res = GestorParametrosIniciales.Instance.servidorPrincipal.restaura_control();
                        if (res)
                        {
                            buttons = MessageBoxButtons.OK;
                            result = MessageBox.Show("Operación realizada con éxito.", caption, buttons);
                        }
                        else
                        {
                            GestorParametrosIniciales.Instance.servidorSecundario.restaura_control();
                            buttons = MessageBoxButtons.OK;
                            result = MessageBox.Show("Se ha producido un error en la transferencia de control. Revise los logs del equipo secundario para ver las causas.", caption, buttons);
                        }
                    }
                }
                else
                {
                    buttons = MessageBoxButtons.OK;
                    result = MessageBox.Show("Deben encontrarse ambos equipos con comunicaciones. No ha podido realizarse la operación.", caption, buttons);

                }


            }

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ControlReplicacionV
{
    public class GestorParametrosIniciales
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const string FORMATO_FECHA = "yyyyMMddHHmmss";

        public static readonly string TEXTO_nombreConcentrador = "nombreConcentrador";
        public static readonly string TEXTO_nombreServidor = "nombreServidor";
        public static readonly string TEXTO_minutosSinConexion = "minutosSinConexion";
        public static readonly string TEXTO_ipServidorPrincipal = "ipServidorPrincipal";
        public static readonly string TEXTO_ipServidorSecundario = "ipServidorSecundario";
        public static readonly string TEXTO_puertoServidorPrincipal = "puertoServidorPrincipal";
        public static readonly string TEXTO_puertoServidorSecundario = "puertoServidorSecundario";
        public static readonly string TEXTO_ipCambioPrincipal = "ipCambioPrincipal";
        public static readonly string TEXTO_baseIpAuxiliares = "ipServidorAuxiliar";

        public static readonly string TEXTO_mascaraRed = "mascaraRed";
        public static readonly string TEXTO_lanzarServicios = "lanzarServicios";
        public static readonly string TEXTO_debug = "debug";

        public static readonly string TEXTO_esServidorPrincipal = "esServidorPrincipal";
        public static readonly string TEXTO_esServidorSecundario = "esServidorSecundario";


        public static readonly string TEXTO_ipDireccionLocal = "ipAddress";

        public static Dictionary<string, Object> parametros = new Dictionary<String, Object>();

        private static GestorParametrosIniciales instance;

        private Servidor _servidorPrincipal;
        private Servidor _servidorSecundario;

        private GestorParametrosIniciales() { }

        public Servidor servidorSecundario
        {
            get => _servidorSecundario;
            set => _servidorSecundario = value;
        }
        public Servidor servidorPrincipal
        {
            get => _servidorPrincipal;
            set => _servidorPrincipal = value;
        }

        public static GestorParametrosIniciales Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GestorParametrosIniciales();
                }
                return instance;
            }
        }

        public Servidor getServidorConexion()
        {
            Servidor serv = null;

            if (_servidorPrincipal != null
            && (_servidorPrincipal.estadoControlador.Equals((int)Servidor.EstadoEquipo.INICIAL)
                || (_servidorPrincipal.estadoControlador.Equals((int)Servidor.EstadoEquipo.COMUNICA))))
                serv = _servidorPrincipal;
            else
                if (_servidorSecundario != null 
            && (_servidorSecundario.estadoControlador.Equals((int)Servidor.EstadoEquipo.INICIAL)
                || (_servidorSecundario.estadoControlador.Equals((int)Servidor.EstadoEquipo.COMUNICA))))
                serv = _servidorSecundario;

            //SI no comunica ninguno asignamos el auxiliar y reiniciamos los demas probocando un ciclo constante de gestion de conexiones
            if (serv == null)
            {
                if (_servidorPrincipal != null)
                {
                    serv = _servidorPrincipal;
                    _servidorSecundario.estadoControlador = (int)Servidor.EstadoEquipo.INICIAL;
                }
                else
                    if (_servidorSecundario != null)
                    serv = _servidorSecundario;

            }

            return serv;
        }

        public Dictionary<string,Object> getParametros()
        {
            return parametros;
        }

        public bool cargaParametros(Object form)
        {

            bool seguir = true;

            //ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "ControlReplicacionV.config" };
            ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "CtrlReplicacion.exe.config" };
            
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

            String ipServidorPrincipal = config.AppSettings.Settings[TEXTO_ipServidorPrincipal].Value;
            String ipServidorSecundario = config.AppSettings.Settings[TEXTO_ipServidorSecundario].Value;
            String ipCambioPrincipal = config.AppSettings.Settings[TEXTO_ipCambioPrincipal].Value;
            String puertoServidorPrincipal = config.AppSettings.Settings[TEXTO_puertoServidorPrincipal].Value;
            String puertoServidorSecundario = config.AppSettings.Settings[TEXTO_puertoServidorSecundario].Value;
            String debugS = config.AppSettings.Settings[TEXTO_debug].Value;


            int cont = 1;
            Queue<string> ipsAuxiliares = new Queue<string>();
            String ipAux;
            bool buscar = true;
            do
            {
                if (config.AppSettings.Settings[TEXTO_baseIpAuxiliares + (cont)] != null)
                    ipsAuxiliares.Enqueue(config.AppSettings.Settings[TEXTO_baseIpAuxiliares + (cont++)].Value);
                else
                    buscar = false;
            } while (buscar);





            String nombreControlador = "";
            if (config.AppSettings.Settings[TEXTO_nombreConcentrador]!=null)
                nombreControlador = config.AppSettings.Settings[TEXTO_nombreConcentrador].Value;

            string nombreServidor = "";
            if (config.AppSettings.Settings[TEXTO_nombreServidor] != null)
                nombreServidor = config.AppSettings.Settings[TEXTO_nombreServidor].Value;

            Boolean esServidorPrincipal = false;
            Boolean esServidorSecundario = false;
            Boolean debugB = false;

            //parametros.Add("ipLocal", "192.168.0.14");


            if (debugS != null && debugS.ToLower().Equals("s"))
            {
                debugB = true;
            }

            if (seguir)
            {
                if (puertoServidorPrincipal != null) parametros.Add(TEXTO_puertoServidorPrincipal, puertoServidorPrincipal);
                if (ipServidorPrincipal != null) parametros.Add(TEXTO_ipServidorPrincipal, ipServidorPrincipal);
                if(ipServidorSecundario!=null) parametros.Add(TEXTO_ipServidorSecundario, ipServidorSecundario);
                if (ipsAuxiliares != null) parametros.Add(TEXTO_baseIpAuxiliares, ipsAuxiliares);
                if (ipCambioPrincipal != null) parametros.Add(TEXTO_ipCambioPrincipal, ipCambioPrincipal);
                if (puertoServidorSecundario != null) parametros.Add(TEXTO_puertoServidorSecundario, puertoServidorSecundario);
                if (nombreControlador != null) parametros.Add(TEXTO_nombreConcentrador, nombreControlador);
                if (nombreServidor != null) parametros.Add(TEXTO_nombreServidor, nombreServidor);
                parametros.Add(TEXTO_debug, debugB);

                // Establish the local endpoint for the socket.  
                // The DNS name of the computer  
                // running the listener is "host.contoso.com".  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

                if (ipServidorPrincipal != null && ipServidorSecundario != null)
                {

                    if(ipServidorPrincipal!=null && puertoServidorPrincipal!=null)
                        _servidorPrincipal = new Servidor((int)Servidor.RolEquipo.PRINCIPAL, ipServidorPrincipal, puertoServidorPrincipal);
                    if (ipServidorSecundario != null && puertoServidorSecundario != null)
                        _servidorSecundario = new Servidor((int)Servidor.RolEquipo.SECUNDARIO, ipServidorSecundario, puertoServidorSecundario);

                }

                //parametros.Add(TEXTO_ipDireccionLocal, ipAddress);
                parametros.Add(TEXTO_esServidorPrincipal, esServidorPrincipal);//Boolean
                parametros.Add(TEXTO_esServidorSecundario, esServidorSecundario);//Boolean

                
                log.Info("Iniciada la ejecución con los siguientes parámetros:");
                foreach (string param in parametros.Keys)
                {
                    String texto = "";
                    if (parametros[param] is string) texto = param + ":" + (string)parametros[param];
                    if (parametros[param] is bool) texto = param + ":" + (bool)parametros[param];
                    if (texto.Length > 0)
                    {
                        log.Info(texto);
                    }
                }

                ((FrmMain)form).AddItemslistParametros(parametros);



            }

            return seguir;
        }
        public bool cambiaParametro(string clave,string valor)
        {
            bool res = false;

            try
            {



                //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //configuration.AppSettings.Settings[clave].Value = valor;
                //configuration.Save();

                //ConfigurationManager.RefreshSection("appSettings");


                ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "ControlReplicacionV.config" };
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                config.AppSettings.Settings[clave].Value = valor;
                config.Save();

            }
            catch (Exception e)
            {
                log.Error(e);
            }


            return res;


        }
        public String dame_cadena_ip_auxiliares()
        {
            String res = "";

            Queue<string> ipsAuxiliares = (Queue<string>)parametros[GestorParametrosIniciales.TEXTO_baseIpAuxiliares];
            if (ipsAuxiliares != null)
            {
                IEnumerable<String> en = ipsAuxiliares.AsEnumerable();
                foreach(string ip in en)
                {
                    if (res.Length > 0) res = res + "-";
                    res = res + ip;
                }

            }

            return res;
        }

    }



}

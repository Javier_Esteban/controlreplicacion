﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using System.Net;
using System.ServiceProcess;
using System.Configuration;
using System.IO;
using System.Reflection;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.properties", Watch = true)]
namespace ControlReplicacionV
{
    static class Program
    {

        //static Thread threadServidorIp = (Thread)null;
        static Thread thControl;
        //static Thread thProcesaComandos;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string Version = String.Format("Versión {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());

        //Para filtrar directamente un servicio:
        //ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == ServiceName);
        //var ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == ServiceName);
        //AssertState.Equal(ServiceControllerStatus.Running, ctl.Status);
        //En otra máquina
        //var ctl = ServiceController.GetServices("M2JESTEBAN").FirstOrDefault(s => s.ServiceName == "Mysql");


        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {


            try
            {
                log.Info("****** Inicio del sistema *******.Version:" + Version);

                
                //CfgControlReplicacion.
                string titulo = "";

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                FrmMain formPpal = new FrmMain();
                //GestorParametrosIniciales.Instance.cambiaParametro("test_servidor", "s");
                log.Debug("Cargamos parametros iniciales.");
                bool seguir = GestorParametrosIniciales.Instance.cargaParametros(formPpal);
                //ControlReplicacionV.Properties.Settings["prueba"]
                if (seguir)
                {
                    ////Lanzamos Thread de comunicaciones
                    ////String testServidor = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_testServidor];
                    ////{
                    //    threadServidorIp = new Thread(ThreadServidorIp.StartListening);
                    //    threadServidorIp.Start();
                    ////}


                    //Lanzamos Thread de control de de los servicios
                    thControl = new Thread(ControlReplicacion.Instance.StartProcess);
                    thControl.Start(formPpal);

                    formPpal.Text = formPpal.Text + "." + Version;
                    if(titulo!=null && titulo.Length>0) formPpal.Text += " - " + titulo + " - ";

                    formPpal.lEstadoEquipoAuxiliar.Text = GestorParametrosIniciales.Instance.dame_cadena_ip_auxiliares();



                    //string ipPrincipal = (string)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorPrincipal];
                    //string ipSecundario = (string)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];
                    //string ipAuxiliar = (string)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorAuxiliar];

                    //if (ipPrincipal != null)
                    //    formPpal.gEquipoPrincipal.Text = formPpal.gEquipoPrincipal.Text + ipPrincipal + ")";
                    //if(ipSecundario!=null)
                    //    formPpal.gEquipoSecundario.Text = formPpal.gEquipoSecundario.Text + ipSecundario + ")";
                    //if(ipAuxiliar!=null)
                    //    formPpal.gEquipoAuxiliar.Text = formPpal.gEquipoAuxiliar.Text + ipAuxiliar + ")";


                    //String testClienteS = ConfigurationManager.AppSettings["test_cliente"];
                    //if (testClienteS != null && testClienteS.Equals("s"))
                    //{
                    //    log.Debug("Activado test de cliente.");
                    //    ClienteIp.StartClient();
                    //    titulo = "Cliente";
                    //}


                    //if(!eslocal)
                    //formPpal.ponPresentacionEquipoSecundario("EQUIPO SECUNDARIO. CONTROL EN REMOTO.");



                    //thProcesaComandos = new Thread(ThreadProcesoComandos.Instance.procesaComandos);
                    //thProcesaComandos.Start(formPpal);


                    Application.Run(formPpal);

                    //if (threadServidorIp != null)
                    //    threadServidorIp.Abort();

                    thControl.Abort();

                    //thProcesaComandos.Abort();

                }

            }
            catch (Exception e)
            {
                log.Error("Error en inicio de programa. Abandomamos su ejecución", e);
                //throw;
            }


        }
        public static void finalizar()
        {

            if(thControl!=null)
                thControl.Abort();

            //if(thProcesaComandos !=null)
            //    thProcesaComandos.Abort();

            //if (threadServidorIp != null)
            //    threadServidorIp.Abort();

        }
    }
}

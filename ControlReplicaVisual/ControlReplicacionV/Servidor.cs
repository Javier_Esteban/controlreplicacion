﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControlReplicacionV
{
    public class Servidor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public enum EstadoEquipo { INICIAL = 0, COMUNICA = 1, SIN_COMUNICACIONES = 2 };
        public enum EstadoServicio { INICIAL = 0, LANZADO = 1, PARADO = 2, SIN_COMUNICACION = 3 };
        public enum RolEquipo { PRINCIPAL = 0, SECUNDARIO = 1};
        public enum RolEjecucion { NORMAL = 0, CONTROL_ADQUIRIDO = 1, CONTROL_CEDIDO = 2, ERROR_CONFIGURACION = 3};


        protected ServiceReference1.Service1Client _protocolo;
        protected String _dirIP;
        protected int _estadoEquipo = (int)EstadoEquipo.INICIAL;
        protected int _estadoControlador = (int)EstadoEquipo.INICIAL;
        protected Dictionary<string, int> _estadoServicios = new Dictionary<string, int>();
        protected Dictionary<string, int> _estadoOtrosServicios = new Dictionary<string, int>();
        protected int _rol = (int)RolEquipo.PRINCIPAL;
        protected int _rolEjecucíon = (int)RolEjecucion.NORMAL;
        protected DateTime _fechaSincronizacion = DateTime.Now;//Ultima llamada correcta
        protected DateTime _ultimoPingOK = DateTime.Now;



        public Servidor(int rol, string dirIP, string puerto, int estadoServicioControlador, int estadoServicioServidor) : this(rol, dirIP, puerto)
        {
        }

        public Servidor(int rol, string dirIP, string puerto)
        {
            this._rol = rol;
            this._dirIP = dirIP;
            try
            {
                _protocolo = new ServiceReference1.Service1Client();
                string endpoint = _protocolo.Endpoint.ToString().Replace("localhost", dirIP);
                // --http://localhost:8733/Mega2/CtrlReplicacion/Service1/
                _protocolo.Endpoint.Address = new EndpointAddress("http://" + dirIP + ":" + puerto + "/Mega2/CtrlReplicacion/Service1/");
                log.Debug("Creado servidor con servicio<" + _protocolo.Endpoint);

            }
            catch (Exception e)
            {
                log.Error("Error en creación de protocolo de servidor con Ip<" + dirIP + ">",e);
            }

        }

        public string dirIP
        {
            get => _dirIP;
            set => _dirIP = value;
        }


        public int estadoEquipo
        {
            get => _estadoEquipo;
            set => _estadoEquipo = value;
        }

        public int estadoControlador
        {
            get => _estadoControlador;
            set => _estadoControlador = value;
        }


        public int rol
        {
            get => _rol;
            set => _rol = value;
        }

        public int rolEjecucion
        {
            get => _rolEjecucíon;
            set => _rolEjecucíon = value;
        }

        public DateTime fechaSincronizacion
        {
            get => _fechaSincronizacion;
            set => _fechaSincronizacion = value;
        }
        public DateTime ultimoPingOK
        {
            get => _ultimoPingOK;
            set => _ultimoPingOK = value;
        }

        public void setEstados(ServiceReference1.EstadoServicios estados)
        {
            if (estados != null)
            {
                this.estadoEquipo = estados.estadoEquipo;
                this._estadoControlador = estados.estadoControlador;
                this._estadoServicios = estados.estadoServicios;
                this._estadoOtrosServicios = estados.estadoOtrosServicios;
            }
            else
            {
                this.estadoEquipo = (int)Servidor.EstadoEquipo.SIN_COMUNICACIONES;
                this._estadoControlador = (int)Servidor.EstadoEquipo.INICIAL;
                this._estadoServicios = new Dictionary<string,int>();
                this._estadoOtrosServicios = new Dictionary<string, int>();
            }

        }

        public ServiceReference1.EstadoServicios dame_estado_servicios(int rol)
        {
            ServiceReference1.EstadoServicios res = null;
            try
            {
                res = _protocolo.dame_estado_servicios(null,rol);
            }
            catch (Exception e)
            {
                if((e is WebException) || e is EndpointNotFoundException)
                {
                    log.Error("Error de conexión, el servicio web puede no estar levantado.", e);
                }
                else
                    log.Error("Error en la obtención del estado de servicios.", e);

                //this.estadoEquipo = (int)EstadoEquipo.SIN_COMUNICACIONES;
                this.estadoControlador = (int)EstadoEquipo.SIN_COMUNICACIONES;
            }
            if (res!=null && res.estadoControlador == (int)EstadoEquipo.SIN_COMUNICACIONES)
            {
                if (ControlReplicacion.hacer_ping(dirIP))
                {
                    this.estadoEquipo = (int)EstadoEquipo.COMUNICA;
                }
                else
                {
                    this.estadoEquipo = (int)EstadoEquipo.SIN_COMUNICACIONES;
                }
            }


            return res;
        }

        public Boolean deten_servicios()
        {

            Boolean res = true;
            try
            {
                //res = _protocolo.denten_servicios();

            }
            catch (Exception e)
            {
                log.Error("Error en la orden de detener servicios.", e);
                res = false;
            }
            return res;
        }

        public Boolean transfiere_control()
        {

            Boolean res = true;
            try
            {
                try
                {
                    //Comprobamos que haya comunicaciones pidiéndo los estados.
                    ServiceReference1.EstadoServicios estado = _protocolo.dame_estado_servicios(null,_rol);
                }
                catch (Exception e)
                {
                    log.Error("Error en la orden de toma de control, operación no realizada.", e);
                    res = false;
                }
                if (res)
                {

                    bool reconecta = false;
                    try
                    {
                        res = _protocolo.transfiere_control();
                    }
                    catch (Exception e)
                    {
                        reconecta = true;
                        log.Error("Error en en la orden de toma de control del servidor<" + this.dirIP + ">", e);
                    }

                    //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
                    if (reconecta)
                    {
                        DateTime milisIni = (DateTime.Now);
                        int milisEspera = 20000;
                        do
                        {
                            res = true;
                            try
                            {
                                _protocolo.dame_estado_servicios(null, (int)Servidor.RolEquipo.PRINCIPAL);

                                res = true;
                            }
                            catch (Exception)
                            {
                                res = false;
                            }

                        } while (!res || ((DateTime.Now - milisIni).TotalMilliseconds < milisEspera)) ;
                    }
                }

            }
            catch (CommunicationException)
            {
                //No hacemos nada, se da pues el equipo secundario cambia de ip y se pierde la comunicación.
            }
            catch (Exception e)
            {
                log.Error("Error en la orden de toma de control.", e);
                res = false;
            }
            return res;
        }

        public Boolean restaura_control()
        {

            Boolean res = true;
            try
            {
                try
                {
                    //Comprobamos que haya comunicaciones pidiéndo los estados.
                    ServiceReference1.EstadoServicios estado = _protocolo.dame_estado_servicios(null, _rol);
                }
                catch (Exception e)
                {
                    log.Error("Error en la orden de toma de control, operación no realizada.", e);
                    res = false;
                }
                if (res)
                {

                    bool reconecta = false;
                    try
                    {
                        res = _protocolo.restaura_control();
                    }
                    catch (Exception e)
                    {

                        reconecta = true;
                    }

                    //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
                    if (reconecta)
                    {

                        DateTime milisIni = (DateTime.Now);
                        int milisEspera = 20000;
                        do
                        {
                            res = true;
                            try
                            {
                                _protocolo.dame_estado_servicios(null, (int)Servidor.RolEquipo.PRINCIPAL);

                                res = true;
                            }
                            catch (Exception)
                            {
                                res = false;
                            }

                        } while (!res || ((DateTime.Now - milisIni).TotalMilliseconds < milisEspera));

                    }
                }

            }
            catch (CommunicationException)
            {
                //No hacemos nada, se da pues el equipo secundario cambia de ip y se pierde la comunicación.
            }
            catch (Exception e)
            {
                log.Error("Error en la orden de toma de control.", e);
                res = false;
            }
            return res;
        }

    }
}

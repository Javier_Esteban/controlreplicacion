﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControlReplicacionV
{


    public class ThreadProcesoComandos
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static ThreadProcesoComandos instance;
        private ComandoEstadoServicios ultimoComandoEstadoServiciosRecibido = null;

        public static ThreadProcesoComandos Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ThreadProcesoComandos();
                }
                return instance;
            }
        }

        public ComandoEstadoServicios UltimoComandoEstadoServiciosRecibido { get => ultimoComandoEstadoServiciosRecibido; set => ultimoComandoEstadoServiciosRecibido = value; }

        public void procesaComandos(object formppal)
        {
            FrmMain form = (FrmMain)formppal;
            do
            {
                try
                {
                    Comando comando = ThreadServidorIp.colaMensajesRecibidos.Take();
                    if (comando != null)
                    {
                        log.Debug("Procesando comando<" + comando.id + ">");
                        switch (comando.id)
                        {
                            case Comando.ID_COMANDO_LATIDO:
                                break;
                            case Comando.ID_COMANDO_LANZAR_ARQUERO:
                                string nombreControlador = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador];
                                ControlReplicacion.Instance.operarServicio(nombreControlador, ControlReplicacion.LANZAR_SERVICIO, "TRANSFERIDO CONTROL DESDE PRINCIPAL.");
                                string nombreServidor = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreServidor];
                                ControlReplicacion.Instance.operarServicio(nombreServidor, ControlReplicacion.LANZAR_SERVICIO, "TRANSFERIDO CONTROL DESDE PRINCIPAL.");
                                break;
                            case Comando.ID_COMANDO_PARAR_ARQUERO:
                                nombreControlador = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreConcentrador];
                                ControlReplicacion.Instance.operarServicio(nombreControlador, ControlReplicacion.PARAR_SERVICIO, "TRANSFERIDO CONTROL DESDE PRINCIPAL.");
                                nombreServidor = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_nombreServidor];
                                ControlReplicacion.Instance.operarServicio(nombreServidor, ControlReplicacion.PARAR_SERVICIO, "TRANSFERIDO CONTROL DESDE PRINCIPAL.");
                                break;
                            case Comando.ID_ESTADO_SERVICIOS:
                                ComandoEstadoServicios comandoEstadoServicios = (ComandoEstadoServicios)comando;

                                //form.label7.setText = comandoEstadoServicios.estadoConcentrador;
                                //form.label9.setText = comandoEstadoServicios.estadoServidor;
                                UltimoComandoEstadoServiciosRecibido = comandoEstadoServicios;
                                if (comandoEstadoServicios.estadoConcentrador!=null && !form.lEstadoServicioRemoto2.Text.Equals(comandoEstadoServicios.estadoConcentrador))
                                    form.SetText(form.lEstadoServicioRemoto2,comandoEstadoServicios.estadoConcentrador);
                                if (comandoEstadoServicios.estadoServidor != null && !form.lEstadoServicioRemoto1.Text.Equals(comandoEstadoServicios.estadoServidor))
                                    form.SetText(form.lEstadoServicioRemoto1, comandoEstadoServicios.estadoServidor);

                                break;
                            case Comando.ID_CAMBIO_IP:

                                string dirIp = ((IPAddress)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipDireccionLocal]).ToString();
                                string ipAux = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                                bool res = ControlReplicacion.CambiarIP(dirIp, ipAux);
                                if (!res)
                                {
                                    log.Error("Error en el proceso de cambio de dirección IP, OPERACIÓN NO REALIZADA.");
                                }
                                break;
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    log.Info("Finalizando proceso ThreadServidorIP.");
                }
                catch (Exception e)
                {
                    log.Error("Se ha producido un error en el proceso del comando.", e);
                }

            } while (true);

        }

    }



}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Concurrent;

namespace ControlReplicacionV
{
    public class ThreadServidorIp
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // Thread signal.  
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public static BlockingCollection<Comando> colaMensajesRecibidos = new BlockingCollection<Comando>();

        public static void StartListening()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];

            
            IPAddress ipAddress = (IPAddress)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipDireccionLocal];
            int puerto = Int32.Parse((String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_puertoServidorPrincipal]);


            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, puerto);

            // Create a TCP/IP socket.  
            //AddressFamily.InterNetwork sería el valor de AddressFamily para Dirección IP versión 4.
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {

                listener.Bind(localEndPoint);
                listener.Listen(100);

                try
                {

                    while (true)
                    {
                        // Set the event to nonsignaled state.  
                        allDone.Reset();

                        // Start an asynchronous socket to listen for connections.  
                        log.Debug("Esperando conexión...");
                        listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                        // Wait until a connection is made before continuing.  
                        allDone.WaitOne();
                    }

                }
                catch (ThreadAbortException)
                {
                    log.Info("Finalizando proceso ThreadServidorIP.");
                }
                catch (Exception e)
                {
                    log.Error("Error en gestión de conexiones Tcp/ip", e);
                }

            }
            catch (Exception e)
            {
                log.Error("Error en comunicaciones. El puerto <" + puerto + "> no puede abrirse, sintoma de que ya está en uso.",e);
                System.Environment.Exit(1);
            }

            //Console.WriteLine("\nPress ENTER to continue...");
            //Console.Read();
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                // Signal the main thread to continue.  
                allDone.Set();

                // Get the socket that handles the client request.  
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = handler;
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,new AsyncCallback(ReadCallback), state);

            }
            catch (Exception e)
            {
                log.Error("Error en la aceptación de callBack", e);
                throw;
            }        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ar"></param>
        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.   
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                content = state.sb.ToString();
                StringReader sr = new StringReader(content);

                XmlSerializer deserializer = new XmlSerializer(typeof(Comando));
                try
                {
                    object comandoRec = deserializer.Deserialize(sr);
                    if (comandoRec is Comando)
                    {
                        ((Comando)comandoRec).fechaRecibido = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                        
                        log.Debug("comando deserializado correctamente:" + content);
                        colaMensajesRecibidos.TryAdd((Comando)comandoRec);
                        //Console.WriteLine("comando deserializado correctamente:"+ content);
                    }
                    else
                    {
                        log.Debug("Recibido comando no serializable, lo eliminamos:" + content);
                        //Console.WriteLine("Recibido comando no esperado:"+content);
                    }

                }
                catch (Exception e)
                {
                    log.Error("Error en recepción de comando", e);
                    //Console.WriteLine(e.ToString());
                }
                //Eliminamos cualquier comando recibido, hay sido o no correcto.
                state.sb.Clear();
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                //// Check for end-of-file tag. If it is not there, read   
                //// more data.  
                //content = state.sb.ToString();
                //if (content.IndexOf("<EOF>") > -1)
                //{
                //    // All the data has been read from the   
                //    // client. Display it on the console.  
                //    Console.WriteLine("Leidos {0} bytes del socket socket. \n Data : {1}",
                //        content.Length, content);
                //    // Echo the data back to the client.  
                //    Send(handler, content);
                //}
                //else
                //{
                //    // Not all data received. Get more.  
                //    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                //    new AsyncCallback(ReadCallback), state);
                //}
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Enviados {0} bytes.", bytesSent);
                log.Debug("Enviados " + bytesSent + " bytes.");

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                log.Error("Error en Envio respuesta a comando", e);
                //Console.WriteLine(e.ToString());
            }
        }
    }
    // State object for reading/writting client data asynchronously  
    public class StateObject
    {
        // Client  socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }

 }

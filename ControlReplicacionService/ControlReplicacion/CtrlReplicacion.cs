﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Threading;
using System.ServiceModel;
using System.Windows.Forms;
using System.Net.Mail;
using System.Collections.ObjectModel;
using System.IO;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.properties", Watch = true)]
namespace CtrlReplicacion
{

    public partial class CtrlReplicacion : InstallableServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Object block = new Object();
        public ServiceHost serviceHost = null;
        private Thread thgestorBackup;
        System.Timers.Timer timer;

        public CtrlReplicacion()
        {
            InitializeComponent();

        }


        public CtrlReplicacion(string[] args)
        {

            //Util.SetComputerName("M2JESTEBAN");
            //Util.reseteaPc();
            //InitializeComponent();


//            eventLog1 = new EventLog();
            log.Warn("***********  LANZANDO SISTEMA **************. Version "+ Application.ProductVersion);


            if (!System.Diagnostics.EventLog.SourceExists("CtrlReplicacion"))
            {
                System.Diagnostics.EventLog.CreateEventSource("CtrlReplicacion", "Application");
            }

            //eventLog1.Source = "CtrlReplicacion";
            //eventLog1.Log = "Application";

            //Process[] processlist = Process.GetProcesses();
            //foreach (Process theprocess in processlist)
            //{
            //    if(theprocess.ProcessName== "ControlReplicacionV")
            //    {
            //        IntPtr hndProccess = theprocess.Handle;
            //        //SendMessage(hndProccess,WM_COPYDATA,(WPARAM)(HWND)hWnd,(LPARAM)(LPVOID) & MyCDS);

            //    }


            //    //Console.WriteLine("Process: {0} ID: {1}", theprocess.ProcessName, theprocess.Id);
            //}



        }

        //******************************  METODOS PARA INSTALACION Y DESINSTALACIÓN DEL SERVICIO.
        /// <summary>
        /// </summary>
        new internal void InstallService()
        {
            base.InstallService();

        }

        new internal void UninstallService()
        {
            base.UninstallService();
        }
        //**************************************

        /// <summary>
        /// </summary>
        /// <param name="args"></param>

        protected override void OnStart(string[] args)
        {

            try
            {
                log.Debug("Iniciando servicio.");

                bool seguir = GestorParametrosIniciales.Instance.cargaParametros();
                if (seguir)
                {
                    //String ipServiciosSupervisadosPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                    //String ipServiciosSupervisadosSecundario = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];
                    //String ipCambioPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];


                    //String nombre = GestorParametrosIniciales.Instance.getNombrePorDireccion(ipServiciosSupervisadosPrincipal);
                    //nombre = GestorParametrosIniciales.Instance.getNombrePorDireccion(ipServiciosSupervisadosSecundario);
                    //nombre = GestorParametrosIniciales.Instance.getNombrePorDireccion(ipCambioPrincipal);


                    bool interfazUp = false;
                    int cont = 0;
                    int numsegundos = 120;
                    do
                    {
                        interfazUp = NetworkManagement.interfaceLevantada(GestorParametrosIniciales.Instance.servidorLocal.dirIP);
                        if (!interfazUp) {
                            log.Error("Error en inicio, el interfaz no esta activo, esperamos " + numsegundos + " segundos.");
                            Thread.Sleep(1000);
                        }

                    } while (!interfazUp && (cont++ < numsegundos));

                    //Chequeamos que no sea un principal lanzado con el secundario con su ip
                    Util.chequeaIpActiva(true);


                    if (serviceHost != null)
                    {
                        serviceHost.Close();
                    }
                    try
                    {
                        // Creamos un ServiceHost para Service1
                        // provide the base address.
                        serviceHost = new ServiceHost(typeof(Service1));
                        //this.eventLog1.WriteEntry("Creado servicio hospedado.", EventLogEntryType.Warning);
                        string uriServicios = "";
                        foreach (Uri colec in serviceHost.BaseAddresses){
                            uriServicios = uriServicios + colec +"\r\n";
                        }

                        log.Debug("Creado servicio hospedado.<" + uriServicios +">");

                    }
                    catch (Exception e)
                    {
                        //this.eventLog1.WriteEntry("Error en hospedaje de servicio. No se puede crear." + e, EventLogEntryType.Error);
                        log.Error("Error en hospedaje de servicio. No se puede crear.",e);
                    }

                    try
                    {
                        // Open the ServiceHostBase to create listeners and start 
                        // listening for messages.
                        serviceHost.Open();
                        //this.eventLog1.WriteEntry("Abierto servicio hospedado.", EventLogEntryType.Warning);
                        log.Debug("Abierto servicio hospedado.");

                    }
                    catch (Exception e)
                    {
                        //this.eventLog1.WriteEntry("Error en hospedaje de servicio. No se puede abrir." + e, EventLogEntryType.Error);
                        log.Error("Error en hospedaje de servicio. No se puede abrir.", e);
                    }


                    // Set up a timer to trigger every minute.  
                    timer = new System.Timers.Timer();
                    int minutosSinConexionInt = GestorParametrosIniciales.Instance.getSegundosLatidoInt();
                    //Configuramos un intervalo de refresto del tiempo de falta de latido 
                    timer.Interval = (GestorParametrosIniciales.Instance.getSegundosLatidoInt() * 1000);
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
                    timer.Start();

                    //Lanzamos thread que ejecuta-restaura copia de seguridad de la BBDD
                    GestorBackup gestorBackup = new GestorBackup();
                    thgestorBackup = new Thread(() => gestorBackup.gestionaBackup());
                    thgestorBackup.Name = "Gestor Backup";
                    log.Debug("lanzamos thread<" + thgestorBackup.Name);
                    thgestorBackup.Start();
                    log.Debug("Thread<" + thgestorBackup.Name + ">ha sido lanzado");

                }
                else
                {
                    System.Environment.Exit(1);
                }

            }
            catch (Exception e)
            {
                log.Error("Error no controlado en inicio de servicio.", e);
            }
        }

        protected override void OnStop()
        {
            log.Debug("recibida notificacion de fin de servicio.");
            log.Debug("Cerramos ServiceHost.");
            if(serviceHost!=null)
                serviceHost.Close();
            log.Debug("Paramos Timer");
            if(timer!=null)
                timer.Stop();
            log.Debug("Paramos gestor de Backup");
            if(thgestorBackup!=null)
                thgestorBackup.Abort();
            //this.eventLog1.WriteEntry("Servicio parado.", EventLogEntryType.Warning);
            log.Debug("Servicio parado.");
        }

        public void chequeaEstadoServicios()
        {
            try
            {

                GestorParametrosIniciales.Instance.servidorLocal.estadoEquipo = (int)Servidor.EstadoEquipo.COMUNICA;
                GestorParametrosIniciales.Instance.servidorLocal.estadoControlador = (int)Servidor.EstadoEquipo.COMUNICA;

                Queue<string> nombresServicios = (Queue<string>)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_base_servicios];
                Queue<string> nombresOtrosServicios = (Queue<string>)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_base_otros_servicios];

                ServiceController[] servicios = ServiceController.GetServices();
                foreach (ServiceController servicio in servicios)
                {
                    if (nombresServicios.Contains(servicio.ServiceName))
                    {
                        //string estado = null;
                        switch (servicio.Status)
                        {
                            case System.ServiceProcess.ServiceControllerStatus.Running:
                                GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.Remove(servicio.ServiceName);
                                GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.Add(servicio.ServiceName,(int)Servidor.EstadoServicio.LANZADO);
                                break;
                            //case System.ServiceProcess.ServiceControllerStatus.Stopped:
                            default:
                                GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.Remove(servicio.ServiceName);
                                GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.Add(servicio.ServiceName, (int)Servidor.EstadoServicio.PARADO);
                                break;

                        }

                    }
                    else
                    {
                        if (nombresOtrosServicios.Contains(servicio.ServiceName))
                        {
                            //string estado = null;
                            switch (servicio.Status)
                            {
                                case System.ServiceProcess.ServiceControllerStatus.Running:
                                    GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.Remove(servicio.ServiceName);
                                    GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.Add(servicio.ServiceName, (int)Servidor.EstadoServicio.LANZADO);
                                    break;
                                //case System.ServiceProcess.ServiceControllerStatus.Stopped:
                                default:
                                    GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.Remove(servicio.ServiceName);
                                    GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.Add(servicio.ServiceName, (int)Servidor.EstadoServicio.PARADO);
                                    break;

                            }

                        }
                    }

                    foreach (string nombreServicio in nombresServicios)
                    {
                        if (!GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.ContainsKey(nombreServicio))
                            GestorParametrosIniciales.Instance.servidorLocal.estadoServicios.Add(nombreServicio, (int)Servidor.EstadoServicio.INICIAL);
                    }
                    foreach (string nombreServicio in nombresOtrosServicios)
                    {
                        if (!GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.ContainsKey(nombreServicio))
                            GestorParametrosIniciales.Instance.servidorLocal.estadoOtrosServicios.Add(nombreServicio, (int)Servidor.EstadoServicio.INICIAL);

                    }

                }

            }
            catch (Exception e)
            {
                log.Error("Error en obtención del estado de los servicios.", e);
            }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {

            if (Monitor.IsEntered(block))
            {
                log.Warn("Monitor bloqueado, esperamos a siguiente ejecución.");
                return;
            }

            Monitor.Enter(block);


            //NetworkManagement.disableEnableNetworkAdapter(GestorParametrosIniciales.Instance.servidorLocal.dirIP);

            //Util.CambiarIP("172.31.0.14", "172.31.0.13");

            int minutosSinConexionInt = GestorParametrosIniciales.Instance.getMinutosSinConexionInt();

            long miliSegundosSinConexion = minutosSinConexionInt * 60 * 1000;
            try
            {
                //Chequeamos los servicios
                solicitaEstadoServicios(GestorParametrosIniciales.Instance.servidorPrincipal);

                solicitaEstadoServicios(GestorParametrosIniciales.Instance.servidorSecundario);

                //Comprobamos que tiene todas las ips activas, en caso de configurar la misma ip en dos pc´s, queda inactiva
                Util.chequeaIpActiva(false);
                //stablecemos un tiempo de estabilización de interfaz de red por si ha habido una inhabilitacion
                //Thread.Sleep(5000);

                switch (GestorParametrosIniciales.Instance.servidorLocal.rol)
                {
                    case (int)Servidor.RolEquipo.PRINCIPAL:

                        switch (GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion)
                        {
                            case (int)Servidor.RolEjecucion.NORMAL://PRINCIPAL-NORMAL

                                if (GestorParametrosIniciales.Instance.servidorPrincipal.hayAlgunServicioParado())
                                {
                                    log.Debug("Detectada situación de emergencia en servidor principal. Se encuentra parado algún servicio necesario.");
                                    Util.enviar_correo_notificacion("Detectada situación de emergencia en servidor principal. Se encuentra parado algún servicio necesario.");
                                    if (GestorParametrosIniciales.Instance.servidorSecundario.estadoEquipo == (int)Servidor.EstadoEquipo.COMUNICA)
                                    {
                                        if((GestorParametrosIniciales.Instance.servidorSecundario.estadoControlador != (int)Servidor.EstadoEquipo.COMUNICA))
                                        {
                                            log.Debug("El secundario comunica pero no tiene el controlador activo. No podemos saber el estado del secundario. Mantenemos la situación actual");
                                            break;
                                        }

                                        //paramos todos los servicios locales
                                        //cambiamos la ip del equipo
                                        //cambiamos el rol de ejecución
                                        Boolean lanzarServicios = (Boolean)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_lanzarServicios];
                                        if (lanzarServicios)
                                        {

                                            DateTime milisIni = (DateTime.Now);
                                            int milisEspera = GestorParametrosIniciales.Instance.getMinutosPreaviso() * 60 * 1000;
                                            Util.enviar_correo_preaviso("causa: servicios en Principal no lanzados");
                                            bool confirmado = true;
                                            do
                                            {

                                                //Durante el tiempo de latido chequeamos que se mantenga la situacion, en caso contrario se cancela la cesion de control.
                                                Util.operarTodosServicios(Util.LANZAR_SERVICIO);

                                                Thread.Sleep(7000);

                                                solicitaEstadoServicios(GestorParametrosIniciales.Instance.servidorPrincipal);

                                                if (!GestorParametrosIniciales.Instance.servidorPrincipal.hayAlgunServicioParado())
                                                {
                                                    log.Debug("Cancelamos activación de servidor secundario: restaurada situación de los servicios.");
                                                    Util.enviar_cancelacion();
                                                    confirmado = false;
                                                }


                                            } while (((DateTime.Now - milisIni).TotalMilliseconds < milisEspera) && confirmado);

                                            if (confirmado)
                                            {

                                                log.Debug("Se activa situación de activación de servidor secundario: uno o ambos servicios parados");


                                                //Nos conectamos con el secundario para notificarle que tome el control
                                                bool transfiereControl = GestorParametrosIniciales.Instance.servidorLocal.toma_control_secundario();

                                                //En la transferencia el secundario ordena la parada de los servicios del primario
                                                //Comprobamos los servicios y si no estan parados asumimos que ha habido un 

                                                if (transfiereControl)
                                                {
                                                    GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
                                                    GestorParametrosIniciales.Instance.servidorSecundario.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                                                    Util.enviar_correo_aviso("causa: servicios en Principal no lanzados");
                                                }
                                                else
                                                {
                                                    log.Debug("Se cancela la cesion de control al seguncario, error en la cesión desde el secundario.");
                                                    try
                                                    {
                                                        Util.operarTodosServicios(Util.LANZAR_SERVICIO);
                                                        //Si se llego a cambiar la ip, restauramos la principal
                                                        string ipServidorPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                                        string ipServidorSecundario = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];
                                                        Util.CambiarIP(ipServidorSecundario, ipServidorPrincipal);
                                                        Util.enviar_cancelacion();

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        log.Error("Error en restauración de escenario tras fallo en cancelacíón.", ex);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            log.Error("Se dan las condiciones, pero no se procede a tomar el control por el secundario pues la configuración lo impide.");

                                        }

                                    }
                                    else
                                    {
                                        //Comprobamos si tenenemos comunicación a las ip auxiliares, en cuyo caso es una caida del secundario.
                                        if (Util.chequeaPingIpAux())
                                        {//Ha caido el secundario

                                            //relanzamos todos los servicios de nuevo para intentar recuperar el que no está lanzado
                                            Util.operarTodosServicios(Util.LANZAR_SERVICIO);

                                        }
                                        else//es el principal el que se ha desconectado de la red
                                        {

                                            if (!Util.comunicacionRedOK())
                                            {
                                                log.Debug("Por perdida de comunicaciones en red, pasamos a estado de CONTROL_CEDIDO.");
                                                Util.operarTodosServicios(Util.PARAR_SERVICIO);
                                                string ipActual = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                                string ipNueva = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                                                bool cambioIp = Util.CambiarIP(ipActual, ipNueva);
                                                if (cambioIp)
                                                    GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;

                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    //En caso de haber perdido comunicaciones, cambiamos la direción IP y paramos los servicios, pasando a control cedido 
                                    if (!Util.comunicacionRedOK())
                                    {
                                        log.Debug("Por perdida de comunicaciones en red, pasamos a estado de CONTROL_CEDIDO.");
                                        Util.operarTodosServicios(Util.PARAR_SERVICIO);
                                        string ipActual = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                        string ipNueva = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                                        bool cambioIp = Util.CambiarIP(ipActual, ipNueva);
                                        if (cambioIp)
                                            GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;

                                    }
                                    else
                                    {
                                        //SI el secundario tiene los servicios lanzados, le decimos que los pare
                                       if ((GestorParametrosIniciales.Instance.servidorSecundario.estadoEquipo == (int)Servidor.EstadoEquipo.COMUNICA)
                                           && (GestorParametrosIniciales.Instance.servidorSecundario.estadoControlador == (int)Servidor.EstadoEquipo.COMUNICA))
                                        {
                                            if (GestorParametrosIniciales.Instance.servidorSecundario.hayAlgunServicioLanzado())
                                            {
                                                //damos la orden al secundario de parar servicios y restaurar configuracion de secundario
                                                GestorParametrosIniciales.Instance.servidorSecundario.restaura_control_principal();
                                            }

                                        }
                                    }

                                    }
                                break;
                            case (int)Servidor.RolEjecucion.CONTROL_CEDIDO://PRINCIPAL-CONTROL_CEDIDO
                                //En caso de cesión por error en ejecución de los servicios, no se cambia de estado, se mantiene hasta que se haga una actuación manual.
                                //Solo se actua si es cesion por perdida de comunicaciones, en cuyo caso el secundario esta como NORMAL.
                                if (GestorParametrosIniciales.Instance.servidorSecundario.rolEjecucion == (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO)
                                    break;

                                solicitaEstadoServicios(GestorParametrosIniciales.Instance.servidorSecundario);

                                //Se ha cedido el control por perdida de comunicaciones, lo recuperamos.
                                if (GestorParametrosIniciales.Instance.servidorSecundario.estadoEquipo == (int)Servidor.EstadoEquipo.COMUNICA)
                                {
                                    if (GestorParametrosIniciales.Instance.servidorSecundario.estadoControlador == (int)Servidor.EstadoEquipo.COMUNICA)
                                    {
                                        log.Debug("Restauramos control de principal al detectar comunicacion con secundario tras cesion de control.");
                                        GestorParametrosIniciales.Instance.servidorSecundario.restaura_control_principal();
                                        //Reactivamos el interfaz de red por si en algún momento ha habido dos ips, reactivar la ip pues se queda sin activar
                                        log.Debug("Activamos desactivamos interfaz de red tras restauracion de control principal por dos equipos con misma ip.");
                                        NetworkManagement.disableEnableNetworkAdapter(GestorParametrosIniciales.Instance.servidorLocal.dirIP);
                                        Thread.Sleep(10000);
                                    }
                                    else
                                    {
                                        log.Debug("El secundario comunica pero no tiene el controlador activo. No podemos saber el estado del secundario. Mantenemos la situación actual");
                                    }
                                }
                                else
                                {

                                    if (Util.chequeaPingIpAux())
                                    {
                                        //ha caido el secundario, no hay ping a secundario pero si a auxiliares, tomamos el control
                                        string ipActual = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                                        string ipNueva = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                        bool cambioIp = Util.CambiarIP(ipActual, ipNueva);
                                        //Aunque no se puedan lanzar todos los servicios, dejamos los que se lancen, pasando a estado de CONTROL
                                        if (cambioIp) {
                                            GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                                            //se lanzan los servicios que se pueda
                                            Util.operarTodosServicios(Util.LANZAR_SERVICIO);
                                            Util.enviar_correo_notificacion("Servidor principal recupera el control.");

                                        }

                                        //Reactivamos el interfaz de red 
                                        log.Debug("Activamos desactivamos interfaz de red tras cambio de ip por reconexion del principal.");
                                        NetworkManagement.disableEnableNetworkAdapter(GestorParametrosIniciales.Instance.servidorLocal.dirIP);
                                        Thread.Sleep(5000);
                                    }

                                }

                                break;
                            default:
                                log.Error("Error, servidor con rol no esperado.<" + GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion + ">");
                                break;
                        }
                        break;
                    case (int)Servidor.RolEquipo.SECUNDARIO:

                        switch (GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion)
                        {
                            case (int)Servidor.RolEjecucion.NORMAL://SECUNDARIO-NORMAL
                                if ((GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)Servidor.EstadoEquipo.SIN_COMUNICACION)
                                    && (GestorParametrosIniciales.Instance.servidorPrincipal.estadoControlador == (int)Servidor.EstadoEquipo.SIN_COMUNICACION)){
                                    log.Debug("Situación de principal sin comunicación, chequeamos resto de red con equipos auxiliares.");
                                    //si hay ping a aux, es que estamos en red y ha caido el principal, tomamos el control por perdida de comunicacion
                                    if (Util.chequeaPingIpAux())
                                    {
                                        Boolean lanzarServicios = (Boolean)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_lanzarServicios];
                                        if (lanzarServicios)
                                        {

                                            DateTime milisIni = (DateTime.Now);
                                            int milisEspera = GestorParametrosIniciales.Instance.getMinutosPreaviso() * 60 * 1000;
                                            bool confirmado = true;
                                            do
                                            {
                                                solicitaEstadoServicios(GestorParametrosIniciales.Instance.servidorPrincipal);
                                                Thread.Sleep(7000);
                                                if ((GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)Servidor.EstadoEquipo.SIN_COMUNICACION)
                                                    && (GestorParametrosIniciales.Instance.servidorPrincipal.estadoControlador == (int)Servidor.EstadoEquipo.SIN_COMUNICACION))
                                                    confirmado = true;
                                                else
                                                    confirmado = false;

                                            } while (((DateTime.Now - milisIni).TotalMilliseconds < milisEspera) && confirmado);

                                            if (confirmado)
                                            {
                                                log.Debug("El principal no contesta, pero los auxiliares si, el secundario toma el control, levantando los servicios.");
                                                string ipActual = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];
                                                string ipNueva = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                                Util.enviar_correo_notificacion("Servidor secundario ha tomado el control por pérdida de comunicaciones con servidor.");
                                                //esperamos unos segundos a que se envie el correo.
                                                Thread.Sleep(5000);
                                                bool cambioIp = Util.CambiarIP(ipActual, ipNueva);
                                                //Aunque no se puedan lanzar todos los servicios, dejamos los que se lancen, pasando a estado de CONTROL
                                                if (cambioIp)
                                                    GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                                                //se lanzan los servicios que se pueda
                                                Util.operarTodosServicios(Util.LANZAR_SERVICIO);

                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    //Si el principal esta lanzado comprobamos que no tengamos 


                                    //Si el principal tuviese los servicios lanzados, paramos los servicios locales
                                    if ((GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)Servidor.EstadoEquipo.COMUNICA) &&
                                        (GestorParametrosIniciales.Instance.servidorPrincipal.estadoControlador == (int)Servidor.EstadoEquipo.COMUNICA))
                                    {
                                        if(GestorParametrosIniciales.Instance.servidorPrincipal.todosServiciosLanzados()){

                                            if (GestorParametrosIniciales.Instance.servidorSecundario.hayAlgunServicioLanzado())
                                            {

                                                log.Debug("Hay comunicacion con el servidor principal, paramos todos los servicios del secundario hasta que el principal tome contorl.");
                                                //Si el principal tiene todos los servicios activos, paramos los servicios locales
                                                Util.operarTodosServicios(Util.PARAR_SERVICIO);
                                            }

                                        }
                                        else
                                        {
                                            log.Debug("El principal tiene algún servicio parado.");
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("El principal no comunica, o su controlador esta parado.");
                                    }

                                }


                                break;
                            case (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO://SECUNDARIO-CONTROL_ADQUIRIDO
                                //En caso de cesión por error en ejecución de los servicios, no se cambia de estado, se mantiene hasta que se haga una actuación manual.
                                //Solo se actua si es cesion por perdida de comunicaciones, en cuyo caso el secundario esta como NORMAL.
                                if (GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion == (int)Servidor.RolEjecucion.CONTROL_CEDIDO)
                                    break;
                                //EN el secundario los servicios estan parados. Tras reinicio de toma de control tenemos que lanzarlos
                                if (GestorParametrosIniciales.Instance.servidorLocal.hayAlgunServicioParado())
                                {
                                    Util.operarTodosServicios(Util.LANZAR_SERVICIO);
                                }


                                if (GestorParametrosIniciales.Instance.servidorPrincipal.estadoEquipo == (int)Servidor.EstadoEquipo.COMUNICA)
                                {
                                    //No hacemos nada, esta en estado de cesion
                                    //En caso de que sea una perdida de comunicaciones, se gestiona desde Servidor, en el momento de la recuperación

                                }
                                else
                                {   //El principal no comunica
                                    //si el equpo no esta en red, paramos los servicios y cambiamos de ip
                                    if (!Util.comunicacionRedOK())
                                    {
                                        string ipActual = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                                        string ipNueva = (string)GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];

                                        Util.enviar_correo_notificacion("Servidor secundario devuelve el control al principal, comunicaciones recuperadas.");
                                        //esperamos unos segundos a que se envie el correo.
                                        Thread.Sleep(5000);


                                        bool cambioIp = Util.CambiarIP(ipActual, ipNueva);
                                        //Aunque no se puedan lanzar todos los servicios, dejamos los que se lancen, pasando a estado de CONTROL
                                        if (cambioIp)
                                            GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;

                                        //se lanzan los servicios que se pueda
                                        Util.operarTodosServicios(Util.PARAR_SERVICIO);
                                    }

                                }

                                break;
                            default:
                                log.Error("Error, servidor con rol de ejecución no esperado.<" + GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion + ">");
                                break;
                        }
                        break;

                }


            }
            catch (Exception e)
            {
                log.Error("Error en control de replicación.", e);
            }

            Monitor.Exit(block);
        }


        private bool solicitaEstadoServicios(Servidor servidor)
        {
            bool hayComunicacion = true;
            if (servidor == null)
                return true;

            if (servidor.Equals(GestorParametrosIniciales.Instance.servidorLocal))
            {
                chequeaEstadoServicios();
            }
            else
            {
                hayComunicacion = Util.hacer_ping(servidor.dirIP);
                if (hayComunicacion)
                {
                    servidor.estadoEquipo = (int)Servidor.EstadoEquipo.COMUNICA;
                    //Comprobamos si hay conexión con el servidor remoto enviandole el comando de estado de Servicios
                    ServiceReference1.EstadoServicios estadoServiciosRemotos = servidor.dame_estado_servicios();

                    if (estadoServiciosRemotos != null)
                    {
                        if (!estadoServiciosRemotos.dirIP.Equals(servidor.dirIP) || estadoServiciosRemotos.rol!=servidor.rol)
                            servidor.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                        else
                        {
                            //Comprobamos si se ha reestableciodo un error de configuración, cambioando configuración en otro equipo.
                            if (GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion == (int)Servidor.RolEjecucion.ERROR_CONFIGURACION)
                                GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;

                            servidor.estadoControlador = (int)Servidor.EstadoEquipo.COMUNICA;
                            servidor.estadoServicios = estadoServiciosRemotos.estadoServicios;
                            servidor.estadoOtrosServicios = estadoServiciosRemotos.estadoOtrosServicios;
                            //if (servidor.rolEjecucion == (int)Servidor.RolEjecucion.NORMAL && estadoServiciosRemotos.rolEjecucion != (int)Servidor.RolEjecucion.NORMAL)
                            //servidor.rolEjecucion = estadoServiciosRemotos.rolEjecucion;

                           if (!servidor.hayAlgunServicioParado())
                            {
                                servidor.fechaUltimoServiciosLanzados = DateTime.Now;

                            }
                        }
                    }
                    else
                    {
                        servidor.estadoControlador = (int)Servidor.EstadoEquipo.SIN_COMUNICACION;
                    }
                }
                else
                {
                    servidor.estadoEquipo = (int)Servidor.EstadoEquipo.SIN_COMUNICACION;
                    servidor.estadoControlador = (int)Servidor.EstadoEquipo.SIN_COMUNICACION;
                }

            }

            return hayComunicacion;
        }

        protected override void OnContinue()
        {
            //eventLog1.WriteEntry("In OnContinue.");
            log.Debug("Continua servicio.");
        }

        public void StartConsola()
        {
                
            OnStart(null);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CtrlReplicacion
{

    class CuentaCorreo
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        string _servidor;
        int _port;
        string _cuenta;
        string _usuario;
        string _clave;

        public CuentaCorreo(string servidor, int port, string cuenta, string usuario, string clave)
        {
            _servidor = servidor;
            _port = port;
            _cuenta = cuenta;
            _usuario = usuario;
            _clave = clave;
        }

        public MailMessage getMensaje(string asunto,string cuerpo,string[]destino)
        {
            MailMessage  res = null;

            if(destino!=null && destino.Length > 0)
            {
                MailMessage email = new MailMessage();
                foreach (string dest in destino)
                {
                    //email.To.Add(new MailAddress("example@example.com"));
                    email.To.Add(new MailAddress(dest));
                }

                //email.From = new MailAddress("example2@example.com");
                email.From = new MailAddress(_cuenta);
                //email.Subject = "Asunto ( " + DateTime.Now.ToString("dd / MMM / yyy hh:mm:ss") + " ) ";
                email.Subject = asunto;
                //email.Body = "Cualquier contenido en <b>HTML</b> para enviarlo por correo electrónico.";
                email.Body = cuerpo;
                email.IsBodyHtml = true;
                email.Priority = MailPriority.Normal;
                res = email;

            }
            return res;
        }

        private SmtpClient getSmtpClient()
        {
            
            SmtpClient smtp = new SmtpClient();
            smtp.Host = _servidor;
            smtp.Port = _port;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new NetworkCredential("email_from@example.com", "contraseña");
            smtp.Credentials = new NetworkCredential(_usuario,_clave );
            return smtp;
        }

        public bool send(MailMessage mensaje)
        {
            bool res = false;
            try {
                this.getSmtpClient().Send(mensaje);
                res = true;
            }catch(Exception e)
            {
                log.Error("Error en envio de correo.", e);
            }
            return res;
    }

    }

}

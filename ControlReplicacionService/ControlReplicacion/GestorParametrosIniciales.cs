﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CtrlReplicacion
{
    public class GestorParametrosIniciales
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const string FORMATO_FECHA = "yyyyMMddHHmmss";

        public static readonly string TEXTO_base_servicios = "nombreServicio";
        public static readonly string TEXTO_base_otros_servicios = "nombreOtroServicio";

        public static readonly string TEXTO_minutosSinConexion = "minutosSinConexion";
        public static readonly string TEXTO_segundosLatido = "segundosLatido";

        public static readonly string TEXTO_ipServidorPrincipal = "ipServidorPrincipal";
        public static readonly string TEXTO_nombreServidorPrincipal = "nombreServidorPrincipal";
        public static readonly string TEXTO_puertoServidorPrincipal = "puertoServidorPrincipal";
        public static readonly string TEXTO_ipServiciosSupervisadosPrincipal = "ipServiciosSupervisadosPrincipal";

        public static readonly string TEXTO_ipServidorSecundario = "ipServidorSecundario";
        public static readonly string TEXTO_nombreServidorSecundario = "nombreServidorSecundario";
        public static readonly string TEXTO_puertoServidorSecundario = "puertoServidorSecundario";
        public static readonly string TEXTO_ipServiciosSupervisadosSecundario = "ipServiciosSupervisadosSecundario";

        public static readonly string TEXTO_baseIpAuxiliares = "ipServidorAuxiliar";

        public static readonly string TEXTO_ipCambioPrincipal = "ipCambioPrincipal";
        public static readonly string TEXTO_nombreCambioPrincipal = "nombreCambioPrincipal";

        public static readonly string TEXTO_mascaraRed = "mascaraRed";
        public static readonly string TEXTO_lanzarServicios = "lanzarServicios";
        public static readonly string TEXTO_debug = "debug";
        public static readonly string TEXTO_nombreInstalacion = "nombreInstalacion";

        public static Dictionary<string, Object> parametros = new Dictionary<String, Object>();

        private static GestorParametrosIniciales instance;

        private Servidor _servidorLocal;

        private Servidor _servidorPrincipal;
        private Servidor _servidorSecundario;
        private Queue<string> _ipsAuxiliares = new Queue<string>();

        public Servidor servidorLocal
        {
            get => _servidorLocal;
            set => _servidorLocal = value;
        }

        public Servidor servidorPrincipal
        {
            get => _servidorPrincipal;
            set => _servidorPrincipal = value;
        }
        public Servidor servidorSecundario
        {
            get => _servidorSecundario;
            set => _servidorSecundario = value;
        }
        public Queue<string> ipsAuxiliares
        {
            get => _ipsAuxiliares;
            set => _ipsAuxiliares= value;
        }


        private GestorParametrosIniciales() { }

        public static GestorParametrosIniciales Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GestorParametrosIniciales();
                }
                return instance;
            }
        }

        public Dictionary<string,Object> getParametros()
        {
            return parametros;
        }

        public String getNombrePorDireccion(String direccion)
        {

            String nombre = null;
            if (direccion == null)
                return null;

            String ipServiciosSupervisadosPrincipal = (String)parametros[TEXTO_ipServiciosSupervisadosPrincipal];
            String ipServiciosSupervisadosSecundario = (String)parametros[TEXTO_ipServiciosSupervisadosSecundario];
            String ipCambioPrincipal = (String)parametros[TEXTO_ipCambioPrincipal];

            if (direccion.Equals(ipServiciosSupervisadosPrincipal)){
                nombre = servidorPrincipal.nombre;
            }
            else
            {
                if (direccion.Equals(ipServiciosSupervisadosSecundario)){
                    nombre = servidorSecundario.nombre;
                }
                else
                {
                    if (direccion.Equals(ipCambioPrincipal))
                    {
                        nombre = (String)parametros[TEXTO_nombreCambioPrincipal];
                    }

                }
            }
            return nombre;
        }

        public Servidor getServidorPorRol(int rol)
        {
            Servidor res = null;

            switch (rol)
            {
                case (int)Servidor.RolEquipo.PRINCIPAL:
                    res = _servidorPrincipal;
                    break;
                case (int)Servidor.RolEquipo.SECUNDARIO:
                    res = _servidorSecundario;
                    break;
            }
            return res;
        }
        public Servidor getServidorPorDirIP(String dirIP)
        {
            Servidor res = null;
            if (_servidorPrincipal.dirIP.Equals(dirIP))
            {
                res = _servidorPrincipal;
            }
            else
            {
                if (_servidorSecundario.dirIP.Equals(dirIP))
                {
                    res = _servidorSecundario;
                }
            }

            return res;
        }


        //public void actualizaRolEjecucionPorIP(IPAddress[] interfaces)
        //{
        //    try
        //    {
        //        String ipServiciosSupervisadosPrincipal = (String)parametros[TEXTO_ipServiciosSupervisadosPrincipal];
        //        String ipServiciosSupervisadosSecundario = (String)parametros[TEXTO_ipServiciosSupervisadosSecundario];
        //        String ipCambioPrincipal = (String)parametros[TEXTO_ipCambioPrincipal];

        //        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
        //        foreach (IPAddress interfaz in interfaces)
        //        {
        //            {
        //                if (interfaz.ToString().Equals(ipServiciosSupervisadosPrincipal))
        //                {
        //                    if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
        //                    {
        //                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
        //                    }
        //                    else
        //                    {//Es un equipo secundario con la ip de servicios del principal
        //                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
        //                    }
        //                    break;
        //                }
        //                else
        //                {

        //                    if (interfaz.ToString().Equals(ipServiciosSupervisadosSecundario))
        //                    {
        //                        if (_servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
        //                        {
        //                            _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
        //                        }
        //                        else
        //                        {
        //                            _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
        //                        }
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (interfaz.ToString().Equals(ipCambioPrincipal))
        //                        {
        //                            if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
        //                            {
        //                                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
        //                            }
        //                            else
        //                            {
        //                                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
        //                            }
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Error en actualizacion de rol de ejecucion", e);
        //    }
        //}
        public void actualizaRolEjecucionPorIP()
        {
            try
            {
                String ipServiciosSupervisadosPrincipal = (String)parametros[TEXTO_ipServiciosSupervisadosPrincipal];
                String ipServiciosSupervisadosSecundario = (String)parametros[TEXTO_ipServiciosSupervisadosSecundario];
                String ipCambioPrincipal = (String)parametros[TEXTO_ipCambioPrincipal];

                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                if (NetworkManagement.ipConfigurada(ipServiciosSupervisadosPrincipal))
                {
                    if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
                    {
                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                    }
                    else
                    {//Es un equipo secundario con la ip de servicios del principal
                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                    }
                }
                else
                {
                    if (NetworkManagement.ipConfigurada(ipServiciosSupervisadosSecundario))
                    {

                        if (_servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
                        {
                            _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                        }
                        else
                        {
                            _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                        }

                    }
                    else
                    {

                        if (NetworkManagement.ipConfigurada(ipCambioPrincipal))
                        {

                            if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
                            {
                                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
                            }
                            else
                            {
                                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                            }

                        }
                    }
                }


                //List<String> listaIPs = NetworkManagement.dame_lista_ips();
                //_servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                //foreach (String interfaz in listaIPs)
                //{
                //    {
                //        if (interfaz.Equals(ipServiciosSupervisadosPrincipal))
                //        {
                //            if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
                //            {
                //                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                //            }
                //            else
                //            {//Es un equipo secundario con la ip de servicios del principal
                //                _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                //            }
                //            break;
                //        }
                //        else
                //        {

                //            if (interfaz.Equals(ipServiciosSupervisadosSecundario))
                //            {
                //                if (_servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
                //                {
                //                    _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                //                }
                //                else
                //                {
                //                    _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                //                }
                //                break;
                //            }
                //            else
                //            {
                //                if (interfaz.Equals(ipCambioPrincipal))
                //                {
                //                    if (_servidorLocal.rol == (int)Servidor.RolEquipo.PRINCIPAL)
                //                    {
                //                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
                //                    }
                //                    else
                //                    {
                //                        _servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.ERROR_CONFIGURACION;
                //                    }
                //                    break;
                //                }
                //            }
                //        }
                //    }
                //}

            }
            catch (Exception e)
            {
                log.Error("Error en actualizacion de rol de ejecucion", e);
            }
        }


        public bool cargaParametros()
        {

            bool seguir = true;

            try { 
                String nombreInstalacion = ConfigurationManager.AppSettings[TEXTO_nombreInstalacion];


                //ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "ControlReplicacionV.config" };
                //Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

                //String nombreControlador = config.AppSettings.Settings[TEXTO_nombreControlador].Value;
                //String nombreServidor = config.AppSettings.Settings[TEXTO_nombreServidor].Value;
                //String minutosSinConexion = config.AppSettings.Settings[TEXTO_minutosSinConexion].Value;
                //String ipServidorPrincipal = config.AppSettings.Settings[TEXTO_ipServidorPrincipal].Value;
                //String ipServidorSecundario = config.AppSettings.Settings[TEXTO_ipServidorSecundario].Value;
                //String ipAuxiliar = config.AppSettings.Settings[TEXTO_ipAuxiliar].Value;
                //String puertoLocal = config.AppSettings.Settings[TEXTO_puertoLocal].Value;
                //String puertoRemoto = config.AppSettings.Settings[TEXTO_puertoRemoto].Value;
                //String mascaraRed = config.AppSettings.Settings[TEXTO_mascaraRed].Value;
                //String lanzarServicios = config.AppSettings.Settings[TEXTO_lanzarServicios].Value;
                //String debugS = config.AppSettings.Settings[TEXTO_debug].Value;

                //Servicios imprescindibles
                Queue<string> servicios = new Queue<string>();
                int cont = 1;
                String nombreServicioLeido;
                bool buscar = true;
                do
                {
                    nombreServicioLeido = ConfigurationManager.AppSettings[TEXTO_base_servicios + (cont++)];
                    if (nombreServicioLeido != null)
                        servicios.Enqueue(nombreServicioLeido);
                    else
                        buscar = false;

                } while (buscar);

                String ipServiciosSupervisadosPrincipal = ConfigurationManager.AppSettings[TEXTO_ipServiciosSupervisadosPrincipal];
                String ipServiciosSupervisadosSecundario = ConfigurationManager.AppSettings[TEXTO_ipServiciosSupervisadosSecundario];


                Queue<string> otrosServicios = new Queue<string>();
                cont = 1;
                buscar = true;
                do
                {
                    nombreServicioLeido = ConfigurationManager.AppSettings[TEXTO_base_otros_servicios + (cont++)];
                    if (nombreServicioLeido != null)
                        otrosServicios.Enqueue(nombreServicioLeido);
                    else
                        buscar = false;

                } while (buscar);

                String minutosSinConexion = ConfigurationManager.AppSettings[TEXTO_minutosSinConexion];
                String segundosLatido = ConfigurationManager.AppSettings[TEXTO_segundosLatido];
                String ipServidorPrincipal = ConfigurationManager.AppSettings[TEXTO_ipServidorPrincipal];
                String nombreServidorPrincipal = ConfigurationManager.AppSettings[TEXTO_nombreServidorPrincipal];
                String puertoServidorPrincipal = ConfigurationManager.AppSettings[TEXTO_puertoServidorPrincipal];
                String ipServidorSecundario = ConfigurationManager.AppSettings[TEXTO_ipServidorSecundario];
                String nombreServidorSecundario = ConfigurationManager.AppSettings[TEXTO_nombreServidorSecundario];
                String puertoServidorSecundario = ConfigurationManager.AppSettings[TEXTO_puertoServidorSecundario];
                String ipCambioPrincipal = ConfigurationManager.AppSettings[TEXTO_ipCambioPrincipal];
                String nombreCambioPrincipal = ConfigurationManager.AppSettings[TEXTO_nombreCambioPrincipal];
                String mascaraRed = ConfigurationManager.AppSettings[TEXTO_mascaraRed];
                String lanzarServicios = ConfigurationManager.AppSettings[TEXTO_lanzarServicios];
                String debugS = ConfigurationManager.AppSettings[TEXTO_debug];

                String ipAux;
                buscar = true;
                cont = 1;
                do
                {
                    ipAux = ConfigurationManager.AppSettings[TEXTO_baseIpAuxiliares + (cont++)];
                    if (ipAux != null)
                        _ipsAuxiliares.Enqueue(ipAux);
                    else
                        buscar = false;

                } while (buscar);


                Boolean lanzarServiciosB = false;
                Boolean debugB = false;

                //parametros.Add("ipLocal", "192.168.0.14");


                if (debugS != null && debugS.ToLower().Equals("s"))
                {
                    debugB = true;
                }

                if (ipServidorPrincipal == null)
                {
                    log.Error("No se ha configurado la dirección IP del servidor principal, se detiene la ejecución");
                    seguir = false;
                }
                if (nombreServidorPrincipal == null)
                {
                    log.Error("No se ha configurado el nombre del servidor principal, se detiene la ejecución");
                    seguir = false;
                }
                if (ipServidorSecundario == null)
                {
                    log.Error("No se ha configurado la dirección IP del servidor secundario, se detiene la ejecución");
                    seguir = false;
                }
                if (nombreServidorSecundario == null)
                {
                    log.Error("No se ha configurado el nombre del servidor secundario, se detiene la ejecución");
                    seguir = false;
                }
                if (ipCambioPrincipal == null)
                {
                    log.Error("No se ha configurado la dirección IPCambioPrincipal, se detiene la ejecución");
                    seguir = false;
                }
                if (nombreCambioPrincipal == null)
                {
                    log.Error("No se ha configurado el nombre nombreCambioPrincipal, se detiene la ejecución");
                    seguir = false;
                }
                if (ipServiciosSupervisadosPrincipal == null)
                {
                    log.Error("No se ha configurado la dirección ipServiciosSupervisadosPrincipal, se detiene la ejecución");
                    seguir = false;
                }
                if (ipServiciosSupervisadosSecundario == null)
                {
                    log.Error("No se ha configurado la dirección ipServiciosSupervisadosSecundario, se detiene la ejecución");
                    seguir = false;
                }
                //Comprobamos que no hay dos direcciones iguales
                bool ipRepetida = true;
                List<String> direcciones = new List<String>();
                direcciones.Add(ipServidorPrincipal);
                if (!direcciones.Contains(ipServidorSecundario))
                {
                    direcciones.Add(ipServidorSecundario);
                    if (!direcciones.Contains(ipCambioPrincipal))
                    {
                        direcciones.Add(ipCambioPrincipal);
                        if (!direcciones.Contains(ipServiciosSupervisadosPrincipal))
                        {
                            if (!direcciones.Contains(ipServiciosSupervisadosSecundario))
                            {
                                ipRepetida = false;
                            }
                        }

                    }
                }
                if (ipRepetida)
                {
                    log.Error("Error en configuración, se han detectado dos direccion iguales, no pueden coincidir entre si.Se detiene la ejecución.");
                    seguir = false;
                }
                if (servicios.Count == 0)
                {
                    log.Info("No se ha configurado ningún servicio a supervisar. Se detiene la ejecución.");
                    seguir = false;
                }

                if (seguir)
                {


                    if (minutosSinConexion == null)
                    {
                        minutosSinConexion = "5";
                        log.Info("No se ha configurado el tiempo sin conexión para lanzar servicio. Se asigna el valor por defecto:" + minutosSinConexion);

                    }
                    if (puertoServidorPrincipal == null)
                    {
                        log.Debug("No se ha configurado el puerto local, se da el valor por defecto.");
                        puertoServidorPrincipal = "11011";
                    }

                    if (puertoServidorSecundario == null)
                    {
                        log.Debug("No se ha configurado el puerto remoto, se da el valor por defecto.");
                        puertoServidorSecundario = "11011";
                    }

                    if (mascaraRed == null)
                    {
                        log.Error("No se ha configurado la máscara de red, se detiene la ejecución");
                        seguir = false;
                    }

                    if (lanzarServicios != null && lanzarServicios.ToLower().Equals("s"))
                    {
                        lanzarServiciosB = true;
                    }

                    parametros.Add(TEXTO_nombreInstalacion, nombreInstalacion);
                    parametros.Add(TEXTO_ipServidorPrincipal, ipServidorPrincipal);
                    parametros.Add(TEXTO_ipServidorSecundario, ipServidorSecundario);
                    parametros.Add(TEXTO_ipCambioPrincipal, ipCambioPrincipal);
                    parametros.Add(TEXTO_nombreServidorPrincipal, nombreServidorPrincipal);
                    parametros.Add(TEXTO_nombreServidorSecundario, nombreServidorSecundario);
                    parametros.Add(TEXTO_nombreCambioPrincipal, nombreCambioPrincipal);
                    parametros.Add(TEXTO_puertoServidorPrincipal, puertoServidorPrincipal);
                    parametros.Add(TEXTO_puertoServidorSecundario, puertoServidorSecundario);
                    parametros.Add(TEXTO_ipServiciosSupervisadosPrincipal, ipServiciosSupervisadosPrincipal);
                    parametros.Add(TEXTO_ipServiciosSupervisadosSecundario, ipServiciosSupervisadosSecundario);
                    parametros.Add(TEXTO_base_servicios, servicios);
                    parametros.Add(TEXTO_base_otros_servicios, otrosServicios);

                    parametros.Add(TEXTO_segundosLatido, segundosLatido);
                    parametros.Add(TEXTO_minutosSinConexion, minutosSinConexion);
                    parametros.Add(TEXTO_mascaraRed, mascaraRed);
                    parametros.Add(TEXTO_debug, debugB);
                    parametros.Add(TEXTO_baseIpAuxiliares, _ipsAuxiliares);

                    // Establish the local endpoint for the socket.  
                    // The DNS name of the computer  
                    // running the listener is "host.contoso.com".  
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

                    if (ipServidorPrincipal != null && nombreServidorPrincipal!=null &&
                        ipServidorSecundario != null && nombreServidorSecundario!=null &&
                        ipCambioPrincipal!=null && nombreServidorPrincipal!=null)
                    {
                        _servidorPrincipal = new Servidor((int)Servidor.RolEquipo.PRINCIPAL, ipServidorPrincipal, puertoServidorPrincipal,nombreServidorPrincipal);
                        _servidorSecundario = new Servidor((int)Servidor.RolEquipo.SECUNDARIO, ipServidorSecundario, puertoServidorSecundario,nombreServidorSecundario);
                        if (NetworkManagement.ipConfigurada(ipServidorPrincipal))
                        {
                            _servidorLocal = _servidorPrincipal;
                            //añadimos a las ip's auxiliares la ip del otro equipo, ya sea principal o secundario
                            _ipsAuxiliares.Enqueue(ipServidorSecundario);

                        }

                        if (NetworkManagement.ipConfigurada(ipServidorSecundario))
                        {
                            _servidorLocal = _servidorSecundario;
                            _ipsAuxiliares.Enqueue(ipServidorPrincipal);

                        }

                        //IPAddress[] interfaces = ipHostInfo.AddressList;
                        //foreach (IPAddress interfaz in interfaces)
                        //{
                        //    if (interfaz.ToString().Equals(ipServidorPrincipal))
                        //    {
                        //        _servidorLocal = _servidorPrincipal;
                        //        //añadimos a las ip's auxiliares la ip del otro equipo, ya sea principal o secundario
                        //        _ipsAuxiliares.Enqueue(ipServidorSecundario);
                        //        break;
                        //    }
                        //    if (interfaz.ToString().Equals(ipServidorSecundario))
                        //    {
                        //        _servidorLocal = _servidorSecundario;
                        //        _ipsAuxiliares.Enqueue(ipServidorPrincipal);
                        //        break;
                        //    }

                        //}
                        //IPAddress[] interfaces = ipHostInfo.AddressList;
                        //if (_servidorLocal != null)
                        //{
                        //    actualizaRolEjecucionPorIP(interfaces);
                        //}
                        if(_servidorLocal!=null)
                            actualizaRolEjecucionPorIP();
                    }
                    if (_servidorLocal == null) {
                        if (!debugB)
                        {
                            log.Info("Error de configuración, el equipo actual no es ninguno de los configurados.");
                            seguir = false;
                        }
                    }
                    else
                    {
                        if (_servidorLocal.rolEjecucion == (int)Servidor.RolEjecucion.ERROR_CONFIGURACION)
                        {
                            log.Info("Error de configuración, el equipo no tiene dos direcciones ip entre las configuradas para control y para servicios.");
                            //seguir = false;
                        }
                    }

                    parametros.Add(TEXTO_lanzarServicios, lanzarServiciosB);//Boolean

                
                    log.Info("Iniciada la ejecución con los siguientes parámetros:");
                    foreach (string param in parametros.Keys)
                    {
                        String texto = "";
                        if (parametros[param] is string) texto = param + ":" + (string)parametros[param];
                        if (parametros[param] is bool) texto = param + ":" + (bool)parametros[param];
                        if (parametros[param] is string[])
                        {
                            texto = param;
                            for (int i = 0; i < ((string[])parametros[param]).Length; i++)
                            {
                                if (((string[])parametros[param])[i]!= null)
                                {
                                    texto += "(" + ((string[])parametros[param])[i] + ")";
                                }
                                else
                                {
                                    break;
                                }

                            }

                        }
                        if (texto.Length > 0)
                        {
                            log.Info(texto);
                        }
                    }

                }
                log.Debug("Carga de parámetros realizada con exito.");

            }
            catch (Exception e)
            {
                seguir = false;
                log.Error("Error en carga inicial de parametros",e);
            }

            return seguir;
        }
        public bool cambiaParametro(string clave,string valor)
        {
            bool res = false;

            try
            {



                //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //configuration.AppSettings.Settings[clave].Value = valor;
                //configuration.Save();

                //ConfigurationManager.RefreshSection("appSettings");


                ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "ControlReplicacionV.config" };
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                config.AppSettings.Settings[clave].Value = valor;
                config.Save();

            }
            catch (Exception e)
            {
                log.Error(e);
            }


            return res;


        }

        public int getMinutosSinConexionInt()
        {
            int minutosSinConexionInt = 10;
            int.TryParse(((String)parametros[GestorParametrosIniciales.TEXTO_minutosSinConexion]), out minutosSinConexionInt);
            return minutosSinConexionInt;
        }

        public int getSegundosLatidoInt()
        {
            int segundosLatido = 20;
            int.TryParse(((String)parametros[GestorParametrosIniciales.TEXTO_segundosLatido]), out segundosLatido);
            return segundosLatido;
        }

        public int getMinutosPreaviso()
        {

            int tiempo = 10;
            try
            {
                String tiempoPreaviso = ConfigurationManager.AppSettings["tiempoPreaviso"];
                if (tiempoPreaviso != null)
                {
                    int.TryParse(tiempoPreaviso, out tiempo);
                }

            }
            catch (Exception e)
            {
                log.Error("Error en la obtención del tiempo de preaviso", e);
            }

            return tiempo;
        }

    }


}

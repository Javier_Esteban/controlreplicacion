﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CtrlReplicacion
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: solicita al remoto el estado de sus servicios
        [OperationContract]
        EstadoServicios dame_estado_servicios(EstadoServicios estadoequipoLLamante,int rol);

        // El principal transfiere el control al secundario por orden del usuario
        [OperationContract]
        Boolean transfiere_control();

        //el principal notifica al secundario que recupera el control
        [OperationContract]
        Boolean restaura_control();


        //el secundario notifica al principal que va a tomar el control y que pare los servicios.
        [OperationContract]
        Boolean denten_servicios();

        //el secundario notifica al principal que lance los servicios para recuperar el control.
        [OperationContract]
        Boolean lanza_servicios();
        
    }

    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    // Puede agregar archivos XSD al proyecto. Después de compilar el proyecto, puede usar directamente los tipos de datos definidos aquí, con el espacio de nombres "CtrlReplicacion.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    [DataContract]
    public class EstadoServicios
    {
        string _dirIP;
        int _estadoEquipo;
        int _estadoControlador;
        Dictionary<string, int> _estadoServicios;
        Dictionary<string, int> _estadoOtrosServicios;
        int _rolEjecucion;
        int _rol;

        [DataMember]
        public int estadoEquipo
        {
            get { return _estadoEquipo; }
            set { _estadoEquipo = value; }
        }
        [DataMember]
        public int estadoControlador
        {
            get { return _estadoControlador; }
            set { _estadoControlador = value; }
        }
        [DataMember]
        public Dictionary<string, int> estadoServicios
        {
            get { return _estadoServicios; }
            set { _estadoServicios = value; }
        }
        [DataMember]
        public Dictionary<string, int> estadoOtrosServicios
        {
            get { return _estadoOtrosServicios; }
            set { _estadoOtrosServicios = value; }
        }


        [DataMember]
        public string dirIP
        {
            get { return _dirIP; }
            set { _dirIP = value; }
        }
        [DataMember]
        public int rol
        {
            get { return _rol; }
            set { _rol = value; }
        }
        [DataMember]
        public int rolEjecucion
        {
            get { return _rolEjecucion; }
            set { _rolEjecucion = value; }
        }

    }
}

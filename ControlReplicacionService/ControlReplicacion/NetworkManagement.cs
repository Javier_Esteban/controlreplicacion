﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;

namespace CtrlReplicacion
{
    class NetworkManagement
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static int CAMBIO_IP_ERROR = 0;
        public static int CAMBIO_IP_REALIZADO = 1;
        public static int CAMBIO_IP_NO_NECESARIO_IP_CONFIGURADA = 1;

        /// <summary> 
        /// Set's a new IP Address and it's Submask of the local machine 
        /// </summary> 
        /// <param name="ip_address">The IP Address</param> 
        /// <param name="subnet_mask">The Submask IP Address</param> 
        /// <remarks>Requires a reference to the System.Management namespace</remarks> 
        public static int setIP(string ipActual, string ip_address, string subnet_mask)
        {
            int hayCambioIp = CAMBIO_IP_ERROR;
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                //if ((bool)objMO["IPEnabled"])
                //{
                try
                {
                    String[] direccionesIp = (String[])objMO["IPAddress"];
                    String[] mascaras = (String[])objMO["IPSubnet"];
                    if (direccionesIp != null)
                    {

                        Queue<string> direccionesIpL = new Queue<string>();
                        Queue<string> mascarasL = new Queue<string>();
                        IPAddress address;
                        int indMask = 0;
                        foreach (String ip0 in direccionesIp)
                        {
                            //Extraemos sólo las direcciones iPV4
                            if (IPAddress.TryParse(ip0, out address))
                            {
                                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    direccionesIpL.Enqueue(ip0);
                                    mascarasL.Enqueue(mascaras[indMask]);
                                }
                            }
                            indMask++;
                        }
                        direccionesIp = direccionesIpL.ToArray();
                        mascaras = mascarasL.ToArray();

                        bool encontrada = false;
                        for (int i = 0; i < direccionesIp.Length; i++)
                        {
                            if (direccionesIp[i].Equals(ipActual))
                            {
                                direccionesIp[i] = ip_address;
                                encontrada = true;
                            }
                        }
                        if (encontrada)
                        {
                            log.Debug("Realizamos el cambio de IP");
                            ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");

                            newIP["IPAddress"] = direccionesIp;
                            newIP["SubnetMask"] = mascaras;

                            ManagementBaseObject setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
                            hayCambioIp = CAMBIO_IP_REALIZADO;
                            log.Debug("Cambio de ip realizado correctamente.Original<"+ ipActual+">Nueva<"+ip_address+">");
                        }
                        else
                        {
                            if (ipConfigurada(ip_address))
                            {
                                log.Debug("No se modifico la dirección IP pues ya estaba configurada.Actual<" + ipActual + ">Nueva<" + ip_address + ">");
                                hayCambioIp = CAMBIO_IP_NO_NECESARIO_IP_CONFIGURADA;
                            }
                            else
                            {
                                log.Error("Error en cambio de ip, no tiene la ip origen ni la destino.Actual<" + ipActual + ">Nueva<" + ip_address + ">");
                                hayCambioIp = CAMBIO_IP_ERROR;
                            }
                        }
                        //foreach(String ipO in direccionesIp)
                        //{
                        //    if (ipO.Equals(ipActual)){


                        //        ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");

                        //        newIP["IPAddress"] = new string[] { ip_address};
                        //        newIP["SubnetMask"] = new string[] {subnet_mask };

                        //        ManagementBaseObject setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
                        //    }
                        //}

                    }

                }
                catch (Exception e)
                {
                    log.Error("Error en el cambio de ip Actual<" + ipActual + ">Nueva<" + ip_address + ">",e);
                }

                //                }
            }
            return hayCambioIp;
        }




        //        public static bool setIP(string ipActual, string ip_address, string subnet_mask)
        //        {
        //            bool hayCambioIp = false;
        //            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
        //            ManagementObjectCollection objMOC = objMC.GetInstances();

        //            foreach (ManagementObject objMO in objMOC)
        //            {
        //                //if ((bool)objMO["IPEnabled"])
        //                //{
        //                    try
        //                    {
        //                        String[] direccionesIp = (String[])objMO["IPAddress"];
        //                        String[] mascaras = (String[])objMO["IPSubnet"];
        //                        if (direccionesIp != null)
        //                        {

        //                            Queue<string> direccionesIpL = new Queue<string>();
        //                            Queue<string> mascarasL = new Queue<string>();
        //                            IPAddress address;
        //                            int indMask = 0;
        //                            foreach (String ip0 in direccionesIp)
        //                            {
        //                                //Extraemos sólo las direcciones iPV4
        //                                if (IPAddress.TryParse(ip0, out address))
        //                                {
        //                                    if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        //                                    {
        //                                        direccionesIpL.Enqueue(ip0);
        //                                        mascarasL.Enqueue(mascaras[indMask]);
        //                                    }
        //                                }
        //                                indMask++;
        //                            }
        //                            direccionesIp = direccionesIpL.ToArray();
        //                            mascaras = mascarasL.ToArray();

        //                            bool encontrada = false;
        //                            for(int i = 0; i < direccionesIp.Length; i++)
        //                            {
        //                                if (direccionesIp[i].Equals(ipActual))
        //                                {
        //                                    direccionesIp[i] = ip_address;
        //                                    encontrada = true;
        //                                }
        //                            }
        //                            if (encontrada)
        //                            {
        //                                ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");

        //                                newIP["IPAddress"] = direccionesIp;
        //                                newIP["SubnetMask"] = mascaras;

        //                                ManagementBaseObject setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
        //                                hayCambioIp = true;
        //                            }
        //                            else
        //                            {

        //                                log.Debug("No se modifico la dirección IP pues ya estaba configurada.<" + ipActual +"><"+ ip_address + ">");
        //                            }
        //                            //foreach(String ipO in direccionesIp)
        //                            //{
        //                            //    if (ipO.Equals(ipActual)){


        //                            //        ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");

        //                            //        newIP["IPAddress"] = new string[] { ip_address};
        //                            //        newIP["SubnetMask"] = new string[] {subnet_mask };

        //                            //        ManagementBaseObject setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
        //                            //    }
        //                            //}

        //                        }

        //                    }
        //                    catch (Exception)
        //                    {
        //                        throw;
        //                    }


        ////                }
        //            }
        //            return hayCambioIp;
        //        }
        /// <summary> 
        /// Set's a new Gateway address of the local machine 
        /// </summary> 
        /// <param name="gateway">The Gateway IP Address</param> 
        /// <remarks>Requires a reference to the System.Management namespace</remarks> 
        public static void setGateway(string gateway)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    try
                    {
                        ManagementBaseObject setGateway;
                        ManagementBaseObject newGateway =
                            objMO.GetMethodParameters("SetGateways");

                        newGateway["DefaultIPGateway"] = new string[] { gateway };
                        newGateway["GatewayCostMetric"] = new int[] { 1 };

                        setGateway = objMO.InvokeMethod("SetGateways", newGateway, null);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
        /// <summary> 
        /// Set's the DNS Server of the local machine 
        /// </summary> 
        /// <param name="NIC">NIC address</param> 
        /// <param name="DNS">DNS server address</param> 
        /// <remarks>Requires a reference to the System.Management namespace</remarks> 
        public static void setDNS(string NIC, string DNS)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        try
                        {
                            ManagementBaseObject newDNS =
                                objMO.GetMethodParameters("SetDNSServerSearchOrder");
                            newDNS["DNSServerSearchOrder"] = DNS.Split(',');
                            ManagementBaseObject setDNS =
                                objMO.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
        }
        /// <summary> 
        /// Set's WINS of the local machine 
        /// </summary> 
        /// <param name="NIC">NIC Address</param> 
        /// <param name="priWINS">Primary WINS server address</param> 
        /// <param name="secWINS">Secondary WINS server address</param> 
        /// <remarks>Requires a reference to the System.Management namespace</remarks> 
        public static void setWINS(string NIC, string priWINS, string secWINS)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        try
                        {
                            ManagementBaseObject setWINS;
                            ManagementBaseObject wins =
                            objMO.GetMethodParameters("SetWINSServer");
                            wins.SetPropertyValue("WINSPrimaryServer", priWINS);
                            wins.SetPropertyValue("WINSSecondaryServer", secWINS);

                            setWINS = objMO.InvokeMethod("SetWINSServer", wins, null);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }
        }

        //public static Boolean ipConfigurada(string dirIp)
        //{
        //    Boolean res = false;
        //    ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
        //    ManagementObjectCollection objMOC = objMC.GetInstances();

        //    foreach (ManagementObject objMO in objMOC)
        //    {
        //        try
        //        {
        //            String[] direccionesIp = (String[])objMO["IPAddress"];
        //            if (direccionesIp != null)
        //            {
        //                IPAddress address;
        //                foreach (String ip0 in direccionesIp)
        //                {
        //                    //Extraemos sólo las direcciones iPV4
        //                    if (IPAddress.TryParse(ip0, out address))
        //                    {
        //                        if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        //                        {
        //                            if (ip0.Equals(dirIp))
        //                            {
        //                                res = true;
        //                                break;
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            log.Error("Error en busqueda de ip.", e);
        //        }

        //    }
        //    return res;
        //}

        public static List<String> dame_lista_ips()
        {
            List<String> res = new List<String>();
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                try
                {
                    String[] direccionesIp = (String[])objMO["IPAddress"];
                    if (direccionesIp != null)
                    {
                        IPAddress address;
                        foreach (String ip0 in direccionesIp)
                        {
                            //Extraemos sólo las direcciones iPV4
                            if (IPAddress.TryParse(ip0, out address))
                            {
                                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    res.Add(ip0);
                                }
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    log.Error("Error en busqueda de ip.", e);
                }

            }
            return res;
        }

        public static Boolean ipConfigurada(string dirIp)
        {
            return (GetNetworkAdapterForIP(dirIp) != null);
        }


        public static NetworkInterface GetNetworkAdapterForIP(String ip)
        {
            try
            {
                NetworkInterface[] niList = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface ni in niList)
                {
                    UnicastIPAddressInformationCollection ipColl = ni.GetIPProperties().UnicastAddresses;
                    foreach (UnicastIPAddressInformation ipInf in ipColl)
                    {
                        if (ipInf.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            String dirIP = ipInf.Address.ToString();
                            if (dirIP.Equals(ip))
                                return ni;
                        }
                    }
                }

            }
            catch (Exception e)
            {
               log.Error("Error en obtencdion de intefaz de red de direccion<" + ip + ">",e);
            }
            return null;
        }

        public static bool disableEnableNetworkAdapter(String ip)
        {
            bool res = false;

            try
            {
                //ManagementObject interfaceIp = GetNetworkInterfaceManagementObject("172.31.0.14");
                NetworkInterface interfaz = GetNetworkAdapterForIP(ip);
                if (interfaz != null)
                {
                    SelectQuery wmiQuery = new SelectQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId != NULL");
                    ManagementObjectSearcher searchProcedure = new ManagementObjectSearcher(wmiQuery);
                    foreach (ManagementObject item in searchProcedure.Get())
                    {
                        //if (((string)item["NetConnectionId"]) == "Ethernet 2")
                        if (((string)item["NetConnectionId"]) == interfaz.Name)
                        {
                            log.Debug("Procedemos a habilitar/inhibir interface de red <" + interfaz.Name +">");
                            item.InvokeMethod("Disable", null);
                            item.InvokeMethod("Enable", null);
                            res = true;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                log.Error("Error en activacion e inhibicion de red <" + ip + ">",e);

            }

            return res;
        }

        public static bool interfaceLevantada(string ip)
        {
            bool res = false;

            try
            {
                //ManagementObject interfaceIp = GetNetworkInterfaceManagementObject("172.31.0.14");
                NetworkInterface interfaz = GetNetworkAdapterForIP(ip);
                if (interfaz != null)
                {
                    if (interfaz.OperationalStatus == OperationalStatus.Up)
                        res = true;
                }
            }
            catch (Exception e)
            {
                log.Error("Error en activacion e inhibicion de red <" + ip + ">", e);

            }

            return res;

        }

    }

}

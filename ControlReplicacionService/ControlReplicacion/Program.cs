﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace CtrlReplicacion
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        static void Main(string[] args)
        {


            if (Environment.UserInteractive)
            {
                FileStream fs = new FileStream("instalacion.log", FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                Console.SetOut(sw);
                if (args.Length == 1)
                {
                    CtrlReplicacion s1 = new CtrlReplicacion();
                    string nombre = ConfigurationManager.AppSettings["nombreServicio"];
                    if (nombre != null)
                    {
                        s1.ServiceName = nombre;
                    }
                    switch (args[0])
                    {
                        case "-install":
                            s1.InstallService();

                            break;
                        case "-uninstall":

                            s1.UninstallService();
                            break;
                        default:
                            Console.WriteLine("Opciones:");
                            Console.WriteLine("       - install");
                            Console.WriteLine("       - uninstall");
                            throw new NotImplementedException();
                    }
                }
                else
                {
                    Console.WriteLine("Opciones:");
                    Console.WriteLine("       - install");
                    Console.WriteLine("       - uninstall");
                }
                sw.Close();


                //Descomentar para hacer pruebas desde entorno Visual Studio
                if (args.Length == 2)
                {
                    CtrlReplicacion ctrlConsola = new CtrlReplicacion(args);
                    ctrlConsola.StartConsola();

                    if (args[0].Equals("entorno"))
                    {
                        do
                        {
                            Thread.Sleep(10000);
                        } while (true);
                    }
                }


            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new CtrlReplicacion(args)
                };
                ServiceBase.Run(ServicesToRun);
            }

        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace CtrlReplicacion
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            //string parameter = "MySource1\" \"MyLogFile1";
            //Context.Parameters["assemblypath"] = "\"" + Context.Parameters["assemblypath"] + "\" \"" + parameter + "\"";
            //modificamos la clave de registro ImagePath, añadiendo el parámetro de la dirección ip de Arquero
            // Context.Parameters["assemblypath"] = "\"" + Context.Parameters["assemblypath"] + "\" \"" + "0.0.0.0" + "\"";
            base.OnBeforeUninstall(savedState);
        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}

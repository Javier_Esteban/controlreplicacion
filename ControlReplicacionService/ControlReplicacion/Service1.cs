﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace CtrlReplicacion
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código y en el archivo de configuración a la vez.
    public class Service1 : IService1
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public EstadoServicios dame_estado_servicios(EstadoServicios estadoequipoLLamante,int rol) {


            if (estadoequipoLLamante != null)
            {
                Servidor llamante = GestorParametrosIniciales.Instance.getServidorPorDirIP(estadoequipoLLamante.dirIP);
                if (llamante != null)
                {
                    llamante.actualizaEstadoServicios(estadoequipoLLamante);
                }
            }

            EstadoServicios res = new EstadoServicios();
            Servidor srv = GestorParametrosIniciales.Instance.getServidorPorRol(rol);
            if (srv != null)
            {

                res.dirIP = srv.dirIP;
                res.estadoEquipo = srv.estadoEquipo;
                res.estadoControlador = srv.estadoControlador;
                res.estadoServicios = srv.estadoServicios;
                res.estadoOtrosServicios = srv.estadoOtrosServicios;
                res.rol = srv.rol;
                res.rolEjecucion = srv.rolEjecucion;

                ////En caso de que el estado solicitado no sea el nuestro(al auxiliar se le puede pedir los servicios del principal o secundario), se hace una solicitud online
                //string dirIp = ((IPAddress)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipDireccionLocal]).ToString();
                //if (srv.dirIP.Equals(dirIp))
                //{
                //    res.dirIP = srv.dirIP;
                //    res.estadoEquipo = srv.estadoEquipo;
                //    res.estadoControlador = srv.estadoControlador;
                //    res.estadoConcentrador = srv.estadoServicioConcentrador;
                //    res.estadoServidor = srv.estadoServicioServidor;
                //    res.rolEjecucion = srv.rolEjecucion;

                //}
                //else
                //{
                //    ServiceReference1.EstadoServicios resOnLine = srv.dame_estado_servicios();
                //    if (resOnLine != null)
                //    {
                //        res.estadoEquipo = resOnLine.estadoEquipo;
                //        res.estadoControlador = resOnLine.estadoControlador;
                //        res.estadoConcentrador = resOnLine.estadoConcentrador;
                //        res.estadoServidor = resOnLine.estadoServidor;
                //        res.dirIP = resOnLine.dirIP;
                //        resOnLine.rolEjecucion = resOnLine.rolEjecucion;
                //    }
                //    else
                //        res = null;
                //}
            }
            else
                res = null;


            return res;
        }

        /// <summary>
        /// Recibimos una solicitud del principal para que como secundario tomemos el control
        /// </summary>
        /// <returns></returns>
        public Boolean transfiere_control()
        {
            Boolean res = true;

            log.Debug("Recibida solicitud remota de transferencia de control a secundario .");

            int cont = 0;
            while (Monitor.IsEntered(CtrlReplicacion.block) && cont++<5){
                Thread.Sleep(1000);
            }
            if (cont < 5)
            {
                Monitor.Enter(CtrlReplicacion.block);

                try
                {
                    res = GestorParametrosIniciales.Instance.servidorSecundario.toma_control_secundario_actuacionLocal();
                }
                catch (Exception e)
                {

                    log.Error("Error no controlado en el proceso de cambio de dirección IP.", e);
                    res = false;
                }

                Monitor.Exit(CtrlReplicacion.block);
            }
            else
            {
                res = false;
                log.Warn("No se puede realizar la transferencia remota de control, proceso bloqueado.");
            }

            return res;
        }

        /// <summary>
        /// Recibimos una solicitud del principal para restaurar el control al principal
        /// </summary>
        /// <returns></returns>
        public Boolean restaura_control()
        {


            
            Boolean res = true;

            log.Debug("Recibida solicitud remota de restauracion de control a principal .");

            int cont = 0;
            while (Monitor.IsEntered(CtrlReplicacion.block) && cont++ < 5)
            {
                Thread.Sleep(1000);
            }
            if (cont < 5)
            {

                try
                {
                log.Debug("Recibida solicitud remota de restauracion de control sobre principal.");
                res = GestorParametrosIniciales.Instance.servidorSecundario.restaura_control_principal_actuacionLocal();
                }
                catch (Exception e)
                {

                    log.Error("Error no controlado en el proceso de cambio de dirección IP.", e);
                    res = false;
                }
            }
            else
            {
                res = false;
                log.Warn("No se puede realizar la restauración remota de control, proceso bloqueado.");
            }

            return res;
        }

        /// <summary>
        /// Recibimos una notificación para detener los servicios ya que el secundario toma el control
        /// </summary>
        /// <returns></returns>
        public Boolean denten_servicios()
        {

            log.Debug("Realizamos operación de detener servicios por solicitud desde equipo remoto.");
            return GestorParametrosIniciales.Instance.servidorLocal.deten_servicios_locales();
        }

        /// <summary>
        /// Recibimos una notificación para detener los servicios ya que el secundario toma el control
        /// </summary>
        /// <returns></returns>
        public Boolean lanza_servicios()
        {
            log.Debug("Realizamos operación de lanzar de servicios por solicitud desde equipo remoto.");
            return GestorParametrosIniciales.Instance.servidorLocal.lanza_servicios_locales();

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CtrlReplicacion
{
    public class Servidor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public enum EstadoEquipo { INICIAL = 0, COMUNICA = 1, SIN_COMUNICACION = 2 };
        public enum EstadoServicio { INICIAL = 0, LANZADO = 1, PARADO = 2, SIN_COMUNICACION = 3 };
        public enum RolEquipo { PRINCIPAL = 0, SECUNDARIO = 1 };
        public enum RolEjecucion { NORMAL = 0, CONTROL_ADQUIRIDO = 1, CONTROL_CEDIDO = 2, ERROR_CONFIGURACION = 3 };


        protected ServiceReference1.Service1Client _protocolo;
        protected String _dirIP;
        protected String _puerto;
        protected String _nombre;
        protected int _estadoEquipo = (int)EstadoEquipo.INICIAL;
        protected int _estadoControlador = (int)EstadoEquipo.INICIAL;

        protected Dictionary<string, int> _estadoServicios = new Dictionary<string, int>();
        protected Dictionary<string, int> _estadoOtrosServicios = new Dictionary<string, int>();


        protected int _rol = (int)RolEquipo.PRINCIPAL;
        protected int _rolEjecucíon = (int)RolEjecucion.NORMAL;
        protected DateTime _ultimoServiciosLanzados = DateTime.Now;//Ultima llamada correcta


        public Servidor(int rol, string dirIP, string puerto,string nombre, Dictionary<string, int> estadoServicios, Dictionary<string, int> estadoOtrosServicios) : this(rol, dirIP, puerto,nombre)
        {
            this._estadoServicios = estadoServicios;
            this._estadoOtrosServicios = estadoOtrosServicios;
        }

        public Servidor(int rol, string dirIP, string puerto, string nombre)
        {
            this._rol = rol;
            this._dirIP = dirIP;
            this._puerto = puerto;
            this._nombre = nombre;
        }


        public string dirIP
        {
            get => _dirIP;
            set => _dirIP = value;
        }
        public string nombre
        {
            get => _nombre;
            set => _nombre = value;
        }
        public string puerto
        {
            get => _puerto;
        }

        protected ServiceReference1.Service1Client getProtocolo()
        {
            if (_protocolo == null)
            {
                if (!this.Equals(GestorParametrosIniciales.Instance.servidorLocal))
                {
                    try
                    {
                        _protocolo = new ServiceReference1.Service1Client();
                        string endpoint = _protocolo.Endpoint.ToString().Replace("localhost", dirIP);
                        // --http://localhost:8733/Mega2/CtrlReplicacion/Service1/
                        _protocolo.Endpoint.Address = new EndpointAddress("http://" + dirIP + ":" + puerto + "/Mega2/CtrlReplicacion/Service1/");
                        log.Debug("Creado servidor con servicio<" + _protocolo.Endpoint.Address);

                    }
                    catch (Exception e)
                    {
                        log.Error("Error en creación de protocolo de servidor con endpoint<" + dirIP + ":" + puerto + ">", e);
                    }
                }
                else
                    log.Debug("Llamada a protocolo de servidor principal con valor null.");
            }
            return _protocolo;

        }


        public int estadoEquipo
        {
            get => _estadoEquipo;
            set => _estadoEquipo = value;
        }

        public int estadoControlador
        {
            get => _estadoControlador;
            set => _estadoControlador = value;
        }

        public Dictionary<string, int> estadoServicios
        {
            get => _estadoServicios;
            set => _estadoServicios = value;
        }

        public Dictionary<string, int> estadoOtrosServicios
        {
            get => _estadoOtrosServicios;
            set => _estadoOtrosServicios = value;
        }

        public int rol
        {
            get => _rol;
            set => _rol = value;
        }

        public int rolEjecucion
        {
            get => _rolEjecucíon;
            set => _rolEjecucíon = value;
        }

        public DateTime fechaUltimoServiciosLanzados
        {
            get => _ultimoServiciosLanzados;
            set => _ultimoServiciosLanzados = value;
        }

        public bool hayAlgunServicioParado()
        {
            bool res = false;

            foreach (int estado in estadoServicios.Values)
            {
                if (estado == (int)EstadoServicio.PARADO)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public bool todosServiciosLanzados()
        {
            bool res = true;

            foreach (int estado in estadoServicios.Values)
            {
                if (estado != (int)EstadoServicio.LANZADO)
                {
                    res = false;
                    break;
                }
            }

            return res;
        }

        public bool hayAlgunServicioLanzado()
        {
            bool res = false;

            foreach (int estado in estadoServicios.Values)
            {
                if (estado == (int)EstadoServicio.LANZADO)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public ServiceReference1.EstadoServicios dame_estado_servicios()
        {
            return dame_estado_servicios(this.rol);
        }

        public ServiceReference1.EstadoServicios dame_estado_servicios(int rol)
        {
            ServiceReference1.EstadoServicios res = null;
            try
            {
                ServiceReference1.EstadoServicios estadoServiciosLocales = GestorParametrosIniciales.Instance.servidorLocal.dame_estado_servicios_actuales();
                res = getProtocolo().dame_estado_servicios(estadoServiciosLocales, rol);

            }
            catch (WebException e)
            {
                switch (((System.Net.WebException)e.InnerException).Status)
                {
                    case (System.Net.WebExceptionStatus.ConnectFailure):
                        log.Error("Error de conexión, el servicio web puede no estar levantado.", e);
                        break;
                    default:
                        log.Error("Error de conexión.", e);
                        break;
                }
            }
            catch (EndpointNotFoundException ep)
            {
                log.Error("Error de comunicaciones en la obtención del estado de servicios <" + this.dirIP + ">" + ep.Message);
            }
            catch (Exception e)
            {

                log.Error("Error en la obtención del estado de servicios del servidor<" + this.dirIP + ">", e);
            }
            return res;
        }

        public ServiceReference1.EstadoServicios dame_estado_servicios_actuales()
        {
            ServiceReference1.EstadoServicios res = new ServiceReference1.EstadoServicios();
            res.estadoEquipo = estadoEquipo;
            res.estadoControlador = estadoControlador;
            res.estadoServicios = estadoServicios;
            res.estadoOtrosServicios = estadoOtrosServicios;

            return res;
        }

        public void actualizaEstadoServicios(EstadoServicios estado)
        {
            _estadoEquipo = (int)EstadoEquipo.COMUNICA;
            _estadoControlador = estado.estadoControlador;
            _estadoServicios = estado.estadoServicios;
            _estadoOtrosServicios = estado.estadoOtrosServicios;

        }
        public Boolean deten_servicios_locales()
        {

            Boolean res = true;

            try
            {
                res = Util.operarTodosServiciosPrincipales(Util.PARAR_SERVICIO);
                Util.operarTodosOtrosServicios(Util.PARAR_SERVICIO);

                string dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                string ipCambioPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                int reintento = 1;
                do
                {
                    res = Util.CambiarIP(dirIp, ipCambioPrincipal);
                    if (!res)
                    {

                        log.Error(String.Format("Error en el proceso de cambio de dirección IP, OPERACIÓN NO REALIZADA.Reintento {0}", reintento));
                        res = Util.operarTodosServiciosPrincipales(Util.LANZAR_SERVICIO);
                        Util.operarTodosOtrosServicios(Util.LANZAR_SERVICIO);
                        res = false;
                    }
                    else
                    {
                        GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
                        GestorParametrosIniciales.Instance.servidorSecundario.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                        log.Debug("Finalizado CON ÉXITO el proceso de detención de servicios.");
                    }

                    if (!res)
                    {
                        Thread.Sleep(10000);
                    }

                } while (!res && reintento++ < 3);

                // Reseteamos el ordenador para restaurar condiciones iniciales ante error en cambio de ipo

                if (!res)
                {
                    var file = new FileInfo("reseteaPc.dat");
                    if (file.Exists)
                    {
                        log.Debug("No reseteamos el equipo porque ya se ha realizado un reseteo y no ha tenido exito.");
                        //Borramos el fichero, en la proxima llamada se volvera a resetear el equipo.
                        file.Delete();
                    }
                    else
                    {
                        log.Debug("No se consigue cambiar la ip del ordenador. Reseteamos para partir de condiciones estables.");
                        //creamos el fichero para marcar que ha habido reseteo
                        using (var myFile = file.Create()) { }
                        Util.reseteaPc();
                    }

                }

            }
            catch (Exception e)
            {

                log.Error("Error no controlado en detención de servicios.", e);
                res = false;
            }


            return res;

        }

        public Boolean deten_servicios_remotos()
        {

            Boolean res = false;

            bool reconecta = false;
            try
            {
                res = getProtocolo().denten_servicios();

            }
            catch (WebException e)
            {
                switch (((System.Net.WebException)e.InnerException).Status)
                {
                    case (System.Net.WebExceptionStatus.ConnectFailure)
:
                        if (this.estadoEquipo.Equals((int)EstadoEquipo.COMUNICA))
                            log.Error("Error de conexión, el servicio web puede no estar levantado.", e);
                        break;
                    default:
                        log.Error("Error de conexión.", e);
                        break;
                }
            }
            catch(TimeoutException te)//caida debida a la reconfiguración del interface de red
            {
                reconecta = true;

            }
            catch (Exception e)
            {

                log.Error("Error en en la orden de detener servicios del servidor<" + this.dirIP + ">", e);
            }

            //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
            if (reconecta)
            {
                Thread.Sleep(15000);
                try
                {
                    getProtocolo().dame_estado_servicios(null,this.rol);

                    res = true;
                }
                catch(Exception e)
                {
                    res = false;
                }
            }



            return res;
        }

        public Boolean lanza_servicios_locales()
        {
            Boolean res = true;

            try
            {
                res = Util.operarTodosServicios(Util.LANZAR_SERVICIO);
                if (res)
                {
                    string dirIp = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                    string ipCambioPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                    if (NetworkManagement.ipConfigurada(ipCambioPrincipal))
                        res = Util.CambiarIP(ipCambioPrincipal, dirIp);
                    else
                    {
                        if (NetworkManagement.ipConfigurada(dirIp))//Ya tiene configurada la ip deseada.
                        {
                            res = true;
                        }
                        else
                        {
                            log.Error("El equipo no tiene configurada ni la ip principal, ni la ip de cambio, error de configuración, no se puede cambiar la ip");
                            res = false;
                        }
                    }
                }

                if (!res)
                {
                    log.Error("Error en el proceso de cambio de dirección IP, OPERACIÓN NO REALIZADA.");
                    res = Util.operarTodosServicios(Util.PARAR_SERVICIO);
                    res = false;
                }
                else
                {
                    GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                    GestorParametrosIniciales.Instance.servidorSecundario.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;

                    log.Debug("Finalizado CON ÉXITO el proceso de lanzar servicios con cambio de ip.");
                }


            }
            catch (Exception e)
            {

                log.Error("Error no controlado en el proceso de lanzar servicios con cambio de ip.", e);
                res = false;
            }


            return res;

        }

        public Boolean lanza_servicios_remotos()
        {

            Boolean res = false;
            bool reconecta = false;
            try
            {
                res = getProtocolo().lanza_servicios();

            }
            catch (WebException e)
            {
                switch (((System.Net.WebException)e.InnerException).Status)
                {
                    case (System.Net.WebExceptionStatus.ConnectFailure)
:
                        if (this.estadoEquipo.Equals((int)EstadoEquipo.COMUNICA))
                            log.Error("Error de conexión, el servicio web puede no estar levantado.", e);
                        break;
                    default:
                        log.Error("Error de conexión.", e);
                        break;
                }
            }
            catch (TimeoutException te)//caida debida a la reconfiguración del interface de red
            {
                reconecta = true;

            }
            catch (Exception e)
            {

                log.Error("Error en en la orden de lanzar servicios del servidor<" + this.dirIP + ">", e);
            }

            //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
            if (reconecta)
            {
                Thread.Sleep(7000);
                try
                {
                    getProtocolo().dame_estado_servicios(null, this.rol);

                    res = true;
                }
                catch (Exception e)
                {
                    res = false;
                }
            }

            return res;
        }


        /**
         *  LLamada remota para tomar el control
         */
        public Boolean toma_control_secundario_actuacionLocal()
        {
            bool res = false;
            if (GestorParametrosIniciales.Instance.servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
            {
                res = toma_control();

            }
            else
            {
                log.Debug("Realizamos operación de tomar control secundario por solicitud desde equipo local.");
                res = deten_servicios_locales();
            }

            return res;

        }

        public Boolean toma_control_secundario()
        {
            bool res = false;
            if (GestorParametrosIniciales.Instance.servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
            {

                //hacemos los cambios en el principal, antes de realizar solicitud sobre el secundario, asi evitamos conflictos de ip
                bool serviciosPPalDetenidos = GestorParametrosIniciales.Instance.servidorPrincipal.deten_servicios_remotos();
                if (serviciosPPalDetenidos)
                {
                    res = toma_control_secundario_actuacionLocal();
                }
                else
                {
                    log.Error("No se produjo la toma de control, el principal no pudo parar los servicios");
                    res = false;
                }

           }
            else
            {

                res = toma_control_secundario_actuacionLocal();
                if (res)
                {
                    bool reconecta = false;
                    try
                    {
                        res = GestorParametrosIniciales.Instance.servidorSecundario.getProtocolo().transfiere_control();

                    }
                    catch (WebException e)
                    {
                        switch (((System.Net.WebException)e.InnerException).Status)
                        {
                            case (System.Net.WebExceptionStatus.ConnectFailure)
        :
                                if (this.estadoEquipo.Equals((int)EstadoEquipo.COMUNICA))
                                    log.Error("Error de conexión, el servicio web puede no estar levantado.<" + this.dirIP + ">", e);
                                break;
                            default:
                                log.Error("Error de conexión.<" + this.dirIP + ">", e);
                                break;
                        }
                    }
                    catch (TimeoutException te)//caida debida a la reconfiguración del interface de red
                    {
                        reconecta = true;

                    }
                    catch (Exception e)
                    {

                        log.Error("Error en en la orden de toma de control del servidor<" + this.dirIP + ">", e);
                    }

                    //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
                    if (reconecta)
                    {
                        Thread.Sleep(7000);
                        try
                        {
                            GestorParametrosIniciales.Instance.servidorSecundario.getProtocolo().dame_estado_servicios(null, (int)Servidor.RolEquipo.SECUNDARIO);

                            res = true;
                        }
                        catch (Exception e)
                        {
                            res = false;
                        }
                    }

                }

            }


            return res;

        }


        /*** El servidor Secundario toma el control, quitándoselo al servidor principal         */
        public Boolean toma_control()
        {
            Boolean res = false;
            try
            {

                log.Debug("Recibida solicitud de toma de control.");
                //Operación solo realizable por el servidor secundario.
                if (!GestorParametrosIniciales.Instance.servidorLocal.rol.Equals((int)Servidor.RolEquipo.SECUNDARIO))
                    return res;

                Servidor servPrincipal = GestorParametrosIniciales.Instance.getServidorPorRol((int)Servidor.RolEquipo.PRINCIPAL);

                string ipServidorPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                string ipServidorSecundario = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];

                //Nos aseguramos que no hay ningún equipo con la ip principal y tomamos el control
                bool comunica = Util.hacer_ping(ipServidorPrincipal);
                if (!comunica)
                {
                    //Cambiamos la IP del equipo
                    res = Util.CambiarIP(ipServidorSecundario, ipServidorPrincipal);
                    if (res)
                    {
                        Util.operarTodosServicios(Util.LANZAR_SERVICIO);

                        log.Warn("Realizado cambio en toma de control de servidor Secundario");

                        GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_CEDIDO;
                        GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.CONTROL_ADQUIRIDO;
                        res = true;

                    }
                    else
                    {
                        res = false;
                        log.Error(String.Format("Error en cambio de ip desde {0} a {1}", ipServidorSecundario, ipServidorPrincipal));
                    }
                }
                else
                {
                    log.Error("Error en proceso de lanzamiento de servicios. No puede realizarse el lanzamiento pues existe otro equipo en la red con la ip<" + ipServidorPrincipal + "");
                    res = false;
                }
            }
            catch (Exception e)
            {
                log.Error("Error al lanzar servicios en equipo auxiliar.", e);
                res = false;
            }
            return res;
        }




        public Boolean restaura_control_principal_actuacionLocal()
        {
            bool res = false;
            if (GestorParametrosIniciales.Instance.servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
            {
                res = restaura_control();

            }
            else
            {
                log.Debug("Realizamos operación de lanzar de servicios por solicitud desde equipo local.");
                res = lanza_servicios_locales();
            }

            return res;

        }
        /**
         * Restauramos el control en el servidor principal.
         **/
        public Boolean restaura_control_principal()
        {
            bool res = false;
            if (GestorParametrosIniciales.Instance.servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO)
            {

                //hacemos los cambios en el principal, antes de realizar solicitud sobre el secundario, asi evitamos conflictos de ip
                bool serviciosPPalLanzados = GestorParametrosIniciales.Instance.servidorPrincipal.lanza_servicios_remotos();
                if (serviciosPPalLanzados)
                {
                    res = restaura_control_principal_actuacionLocal();
                }
                else
                {
                    log.Error("No se produjo la toma de control, el principal no pudo parar los servicios");
                    res = false;
                }

            }
            else
            {
                //Primero damos orden de parar los servicios remotos y la ip remota, para no coincidir la  ip de ambos equipos 

                bool reconecta = false;
                try
                {
                    res = getProtocolo().restaura_control();

                }
                catch (WebException e)
                {
                    switch (((System.Net.WebException)e.InnerException).Status)
                    {
                        case (System.Net.WebExceptionStatus.ConnectFailure)
    :
                            if (this.estadoEquipo.Equals((int)EstadoEquipo.COMUNICA))
                                log.Error("Error de conexión, el servicio web puede no estar levantado.<" + this.dirIP + ">", e);
                            break;
                        default:
                            log.Error("Error de conexión.<" + this.dirIP + ">", e);
                            break;
                    }
                }
                catch (TimeoutException te)//caida debida a la reconfiguración del interface de red
                {
                    reconecta = true;

                }
                catch (Exception e)
                {

                    log.Error("Error en en la orden de toma de control del servidor<" + this.dirIP + ">", e);
                }

                //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
                if (reconecta)
                {
                    int cont = 1;
                    log.Debug("Esperamos la restauracion del interfaz de red antes de solicitar el estado del principal");
                    do
                    {
                        Thread.Sleep(5000);
                        try
                        {
                            GestorParametrosIniciales.Instance.servidorSecundario.dame_estado_servicios();
                            res = true;
                        }
                        catch (Exception e)
                        {
                            log.Error("Error en solicitud de estado a servdidor <" + this.dirIP + ">", e);
                            res = false;
                        }

                    } while (!res && cont++ < 3);
                }
                if (res)
                {
                    res = restaura_control_principal_actuacionLocal();
                }

            }
            return res;
        }

        /*** El servidor Secundario restaura el control, retomándolo el principal         */
        public Boolean restaura_control()
        {
            Boolean res = false;
            bool reconecta = false;
            try
            {

                log.Debug("Recibida solicitud de restauración de control.");
                //Operación solo realizable por el servidor secundario.
                if (!(GestorParametrosIniciales.Instance.servidorLocal.rol == (int)Servidor.RolEquipo.SECUNDARIO))
                    return res;

                Servidor servPrincipal = GestorParametrosIniciales.Instance.getServidorPorRol((int)Servidor.RolEquipo.PRINCIPAL);

                string ipServidorPrincipal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                string ipServidorSecundario = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];

                //Nos aseguramos que no hay ningún equipo con la ip secundaria y tomamos el control
                //Chequeamos si el este equipo ya tiene la ip, en cuyo caso seguimos adelante.
                bool equipoConIp = Util.equipoConIp(ipServidorSecundario);
                if (equipoConIp || !Util.hacer_ping(ipServidorSecundario))
                {
                    //Cambiamos la IP del equipo secundario actual, recuperando la original antes de pedir al principal que recupere esta direccion
                    res = equipoConIp || Util.CambiarIP(ipServidorPrincipal, ipServidorSecundario);
                    if (res)
                    {
                        //paramos los servicios locales
                        Util.operarTodosServicios(Util.PARAR_SERVICIO);
                        log.Warn("Realizada restauración de control sobre el servidor Principal");

                        GestorParametrosIniciales.Instance.servidorPrincipal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                        GestorParametrosIniciales.Instance.servidorLocal.rolEjecucion = (int)Servidor.RolEjecucion.NORMAL;
                        res = true;
                    }
                    else
                    {
                        res = false;
                        log.Error(String.Format("Error en cambio de ip desde {0} a {1}", ipServidorPrincipal,ipServidorSecundario));
                    }
                }
                else
                {
                    log.Error("Error en proceso de restauracion de contron por principal, no puede realizarse pues existe otro equipo en la red con la ip<" + ipServidorSecundario + "");
                    res = false;
                }



            }
            catch (TimeoutException te)//caida debida a la reconfiguración del interface de red
            {
                reconecta = true;

            }
            catch (Exception e)
            {
                log.Error("Error en restauracion de control.", e);
                res = false;
            }

            //Si se ha caido tras la solicitud, esperamos e intentamos reconectar para comprobar que se ha levantado el interfaz de red
            if (reconecta)
            {
                Thread.Sleep(7000);
                try
                {
                    getProtocolo().dame_estado_servicios(null, this.rol);

                    res = true;
                }
                catch (Exception e)
                {
                    res = false;
                }
            }
            return res;
        }

    }


}

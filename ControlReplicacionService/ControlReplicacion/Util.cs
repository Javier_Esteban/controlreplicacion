﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CtrlReplicacion
{
    class Util
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected static bool hayPinAux = true;
        protected static DateTime _ultimoPingOK = DateTime.Now;

        public static bool CambiarIP(string ipActual, string ipNueva)
        {
            return CambiarIP(ipActual, ipNueva, true);
        }


        public static bool CambiarIP(string ipActual, string ipNueva,Boolean cambiarNombre)
        {
            bool res = true;

            string mascaraRed = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_mascaraRed];
            try
            {
                log.Debug("realizamos cambio de ip. Ip actual<" + ipActual + ">ip nueva<" + ipNueva + ">");
                int res_cambioIp = NetworkManagement.setIP(ipActual, ipNueva, mascaraRed);
                //En caso de realizar el cambio de ip, cambiamos el nombre del equipo y reseteamos
                if (cambiarNombre)
                {
                    if (res_cambioIp == NetworkManagement.CAMBIO_IP_REALIZADO)
                    {
                        String nombreNuevo = (String)GestorParametrosIniciales.Instance.getNombrePorDireccion(ipNueva);
                        log.Debug("Realizamos cambio de nombre. Nombre Actual:" + GestorParametrosIniciales.Instance.servidorLocal.nombre + ">nombre Nuevo<" + nombreNuevo + ">");
                        res = SetComputerName(nombreNuevo);
                        if (!res)
                        {
                            log.Debug("Error en cambio de nombre, restauramos ip original.");
                            CambiarIP(ipNueva, ipActual, false);
                        }
                        else
                        {
                            log.Debug("Reseteamos el ordenador tras cambio de ip y nombre.");
                            reseteaPc();
                        }

                    }
                    else
                        res = false;
                }

                if (res)
                {//esperamos a que se estabilice la red tras la reprogramación del interface de red.

                    Thread.Sleep(4000);
                    if (!NetworkManagement.ipConfigurada(ipNueva))//Ya tiene configurada la ip deseada.
                    {
                        res = false;
                        log.Error("tras cambio de ip no se encuentra la nueva ip configurada, error en proceso de<" + ipActual + ">a<" + ipNueva + ">");
                    }
                    //if (!hacer_ping(ipNueva))
                    //{
                    //    res = false;
                    //    log.Error("tras cambio de ip no hay ping a la nueva ip, error en proceso de<" + ipActual + ">a<" + ipNueva + ">");
                    //}
                }
                else
                    log.Error("No se pudo realizar el cambio de ip de<" + ipActual +">a<"+ipNueva +">");

            }
            catch (Exception e)
            {
                log.Error("Error en cambio de ip.", e);
                res = false;
            }

            return res;
        }


        public const int LANZAR_SERVICIO = 0;
        public const int PARAR_SERVICIO = 1;
        public static bool operarServicio(String nombreServicio, int operacion)
        {
            bool res = true;
            int cont = 0;

            do
            {
                res = true;
                try
                {
                    //lanzamos servicio
                    ServiceController[] servicios = ServiceController.GetServices();
                    foreach (ServiceController servicio in servicios)
                    {
                        if (servicio.ServiceName.Equals(nombreServicio))
                        {
                            switch (operacion)
                            {
                                case LANZAR_SERVICIO:
                                    if (servicio.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                                    {
                                        log.Info("Lanzamos el servicio <" + servicio.ServiceName + ">");
                                        servicio.Start();
                                    }
                                    else
                                    {
                                        log.Info("No se lanza el servicio<" + servicio.ServiceName + ">.No esta parado.");
                                    }

                                    break;
                                case PARAR_SERVICIO:
                                    if (servicio.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                                    {
                                        log.Info("Paramos el servicio <" + servicio.ServiceName + ">");
                                        servicio.Stop();
                                    }
                                    else
                                    {
                                        log.Info("No se para el servicio<" + servicio.ServiceName + ">.Ya está parado.");
                                    }
                                    break;
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    string operacionStr;
                    if (operacion == PARAR_SERVICIO)
                        operacionStr = "parar";
                    else
                        operacionStr = "lanzar";
                    log.Error("Error en operación " + operacionStr + " sobre el servicio<" + nombreServicio + ">.Reintento<"+cont+">.", e);
                    res = false;
                    Thread.Sleep(5000);
                }

            } while (!res && cont++ < 3);

            return res;

        }
        public static bool operarTodosServiciosPrincipales(int operacion)
        {
            string textoOperacion = "";
            if (operacion == LANZAR_SERVICIO)
                textoOperacion = "lanzamiento";
            else
                textoOperacion = "detencion";

            bool resLanzar = false;
            try
            {
                Queue<string> servicios = (Queue<string>)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_base_servicios];
                IEnumerable servRecorrer;
                if(operacion==LANZAR_SERVICIO)
                    servRecorrer = servicios;
                else
                    servRecorrer = servicios.Reverse();

                foreach (String nombreServicio in servRecorrer)
                {
                    if (nombreServicio == null)break;

                    resLanzar = Util.operarServicio(nombreServicio, operacion);
                    if (resLanzar)
                        log.Warn("Realizado " + textoOperacion + " de servicio<" + nombreServicio + ">");
                    else
                        log.Error("Error " + textoOperacion + " del servicio<" + nombreServicio + ">");
                }


            }
            catch (Exception e)
            {
                log.Error("Error en operación de  " + textoOperacion + " de  servicios principales.", e);
            }
            return resLanzar;
        }

        public static bool operarTodosOtrosServicios(int operacion)
        {
            bool resLanzar = true;

            string textoOperacion = "";
            if (operacion == LANZAR_SERVICIO)
                textoOperacion = "lanzamiento";
            else
                textoOperacion = "detencion";

            try
            {
                Queue<string> otrosServicios = (Queue<string>)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_base_otros_servicios];
                foreach (String nombreServicio in otrosServicios)
                {
                    if (nombreServicio == null) break;

                    resLanzar = Util.operarServicio(nombreServicio, operacion);
                    if (resLanzar)
                        log.Warn("Realizado " + textoOperacion + " de servicio<" + nombreServicio + ">");
                    else
                        log.Error("Error en " + textoOperacion + " del servicio<" + nombreServicio + ">");
                }

            }
            catch (Exception e)
            {
                log.Error("Error en operación de  " + textoOperacion + " de  servicios", e);
            }

            return resLanzar;
        }

        public static bool operarTodosServicios(int operacion)
        {
            bool res = true;
            res = operarTodosServiciosPrincipales(operacion);
            operarTodosOtrosServicios(operacion);

            return res;
        }

        public static bool hacer_ping(string destino)
        {
            bool res = false;


                int cont = 1;
            do
            {
                try
                {
                    Ping HacerPing = new Ping();
                    int iTiempoEspera = 1000;
                    PingReply RespuestaPing;
                    //string sDireccion;
                    RespuestaPing = HacerPing.Send(destino, iTiempoEspera);
                    if (RespuestaPing.Status == IPStatus.Success)
                    {
                        //txtLog.AppendText("Ping a " +sDireccion.ToString() +"[" +RespuestaPing.Address.ToString() +"]" +" Correcto" +" Tiempo de respuesta = " +RespuestaPing.RoundtripTime.ToString() +" ms" +"\n")
                        res = true;
                    }
                    else
                    {
                        log.Error("No hay ping a equipo<" + destino + ">.Reintento<"+ cont +">Causa<" + RespuestaPing.Status.ToString());
                    }

                }
                catch (Exception e)
                {
                    log.Error("Error en ejecución de ping sobre equipo<" + destino + ">.Reintento<" + cont + ">", e);
                    res = false;
                    Thread.Sleep(4000);
                }

            } while (!res && cont++ < 6);


            return res;
        }

        public static void enviar_correo_preaviso(string observaciones)
        {

            int tiempo = GestorParametrosIniciales.Instance.getMinutosPreaviso();

            String mensaje = "¡¡¡¡¡¡¡¡¡¡ INSTALACIÓN DE " + (String)(GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_nombreInstalacion])  + " !!!!!!!!!!!!! \n" +
                            "En " + tiempo + " minutos el servidor secundario procedera a tomar el control.";
            mensaje = "<h2><u> AVISO DESDE INSTALACION DE " + (String)(GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_nombreInstalacion]) + " </u><br></h2>";
            mensaje = mensaje + "<br> En " + tiempo + " minutos el servidor secundario procedera a tomar el control.</br>";
            if(observaciones!=null && observaciones.Length>0)
                mensaje = mensaje + "<br> Causa " + observaciones + ".</br>";
            mensaje = mensaje + "<br><b><br><br></b><br><b><i> Mensaje generado por el gestor de replicacion de MEGA2.</i></b> <br><b> No responda a este mensaje, esta dirección de correo esta destinada exclusivamente al envío de información.</b><br>";

            envia_correo("Aviso de activación de replicación.", mensaje);

        }
        public static void enviar_cancelacion()
        {
            String mensaje = "<h2><u> AVISO DESDE INSTALACION DE " + (String)(GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_nombreInstalacion]) + " </u><br></h2>";
            mensaje = mensaje + "<br> SE HA CANCELADO la toma de control del servidor secundario. El principal sigue teniendo el control.</br>";
            mensaje = mensaje + "<br><b><br><br></b><br><b><i> Mensaje generado por el gestor de replicacion de MEGA2.</i></b> <br><b> No responda a este mensaje, esta dirección de correo esta destinada exclusivamente al envío de información.</b><br>";


            envia_correo("Aviso de activación de replicación.", mensaje);

        }

        public static void enviar_correo_aviso(string observaciones)
        {
            String nombreInstalacion = (String)(GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_nombreInstalacion]);
            String mensaje = "<h2><u> AVISO DESDE INSTALACION DE " + nombreInstalacion + " </u><br></h2>";
            mensaje = mensaje + "<br> El servidor secundario HA PROCEDIDO A TOMAR EL CONTROL.</br>";
            if (observaciones != null && observaciones.Length > 0)
                mensaje = mensaje + "<br> Causa " + observaciones + ".</br>";
            mensaje = mensaje + "<br><b><br><br></b><br><b><i> Mensaje generado por el gestor de replicacion de MEGA2.</i></b> <br><b> No responda a este mensaje, esta dirección de correo esta destinada exclusivamente al envío de información.</b><br>";


            envia_correo(nombreInstalacion + ": Aviso de activación de replicación.", mensaje);

        }

        public static void enviar_correo_notificacion(string notificacion)
        {
            String nombreInstalacion = (String)(GestorParametrosIniciales.parametros[GestorParametrosIniciales.TEXTO_nombreInstalacion]);

            String mensaje = "<h2><u> AVISO DESDE INSTALACION DE " + nombreInstalacion + " </u><br></h2>";
            mensaje = mensaje + "<br> .</br>";
            if (notificacion != null && notificacion.Length > 0)
                mensaje = mensaje + "<br>  " + notificacion + ".</br>";
            mensaje = mensaje + "<br><b><br><br></b><br><b><i> Mensaje generado por el gestor de replicacion de MEGA2.</i></b> <br><b> No responda a este mensaje, esta dirección de correo esta destinada exclusivamente al envío de información.</b><br>";

            envia_correo(nombreInstalacion + ": Aviso desde gestor de replicación.", mensaje);
        }

        public static void envia_correo(string asunto,string cuerpo)
        {
            String destinosNotificaciones = ConfigurationManager.AppSettings["correosNotificaciones"];
            if (destinosNotificaciones != null && destinosNotificaciones.Trim().Length>0)
            {
                string[] destinos = destinosNotificaciones.Split(';');
                //string[] destinos = { "fjavierEsteban@mega2seguridad.com" };
                if (destinos.Length > 0)
                {
                    string nombreCuenta = null;
                    try
                    {
                        nombreCuenta = ConfigurationManager.AppSettings["nombreCuenta"];
                    }catch { };
                    if(nombreCuenta==null)
                        nombreCuenta = "mantenimiento.hermosilla.112@gmail.com";
                    string nombreUsuario = null;
                    try
                    {
                        nombreUsuario = nombreCuenta.Split('@')[0];
                    }catch { };
                    if (nombreUsuario==null)
                        nombreUsuario = "mantenimiento.hermosilla.112";


                    string clave = null;
                    try
                    {
                        clave = ConfigurationManager.AppSettings["claveCuenta"];
                    }
                    catch { };

                    if (clave == null)
                        clave = "Mega-D0$"; 


                    CuentaCorreo correo = new CuentaCorreo("smtp.gmail.com", 587, nombreCuenta, nombreUsuario, clave);
                    MailMessage mailMessage = correo.getMensaje(asunto, cuerpo, destinos);
                    correo.send(mailMessage);
                }
                else
                {
                    log.Warn("Correo no enviado, no existe ninguna cuenta destino configurada");
                }
            }
            else {
                log.Warn("Correo no enviado, no hay ninguna cuenta destino configurada");
            }

        }
        public static bool equipoConIp(String dirIp)
        {
            bool res = false;

            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

            IPAddress[] interfaces = ipHostInfo.AddressList;
            foreach (IPAddress interfaz in interfaces)
            {
                if (interfaz.ToString().Equals(dirIp))
                {
                    res = true;
                    break;
                }
            }
            return res;
        }

        public static Boolean chequeaPingIpAux()
        {

            if (GestorParametrosIniciales.Instance.ipsAuxiliares != null && GestorParametrosIniciales.Instance.ipsAuxiliares.Count > 0)
            {
                bool hayPinAuxOld = hayPinAux;
                hayPinAux = false;
                //Comprobamos que hay ping a algun servicio auxiliar
                foreach (string ipAux in GestorParametrosIniciales.Instance.ipsAuxiliares)
                {
                    hayPinAux = Util.hacer_ping(ipAux);
                    if (hayPinAux)
                    {
                        _ultimoPingOK = DateTime.Now;
                        break;
                    }
                }

                if (hayPinAux != hayPinAuxOld)
                {
                    if (hayPinAux)
                    {//escenario de recuperación de comunicaciones
                        log.Error("Equipo en red, se han recuperado las comunicaciones.");
                    }
                    else//escenario de pérdida de comunicaciones
                    {
                        log.Error("Error de comunicaciones. No hay comunicación con ningún equipo.");
                    }
                }


            }
            else
            {
                log.Debug("No se configuraron equipos auxiliares, se considera que no hay comunicación.");
                hayPinAux = false;
            }
            return hayPinAux;
        }

        public static bool comunicacionRedOK()
        {
            bool res = true;

            bool pinAux = chequeaPingIpAux();
            if (!pinAux)
            {

                int minutosSinConexionInt = GestorParametrosIniciales.Instance.getMinutosSinConexionInt();
                long miliSegundosSinConexion = minutosSinConexionInt * 60 * 1000;
                if (((DateTime.Now - _ultimoPingOK).TotalMilliseconds > miliSegundosSinConexion))
                {
                    log.Debug("Se ha superado el timeout de comunicaciones. Perdida de comunicaciones con la red");
                    res = false;
                }
            }

            return res;
        }

        public static void chequeaIpActiva(bool cambiaIp)
        {

            String ipConf = null;
            String ipControl = null;
            String ipControlOtro = null;
            String ipCambio = null;
            switch (GestorParametrosIniciales.Instance.servidorLocal.rol)
            {
                case (int)Servidor.RolEquipo.PRINCIPAL:
                    ipControl = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorPrincipal];
                    ipControlOtro = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];

                    String ipServiciosSupervisados = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                    String ipCambioPPal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipCambioPrincipal];
                    if (NetworkManagement.ipConfigurada(ipServiciosSupervisados))
                    {
                        ipConf = ipServiciosSupervisados;
                        ipCambio = ipCambioPPal;
                    }
                    else
                    {
                        if (NetworkManagement.ipConfigurada(ipCambioPPal))
                        {
                            ipConf = ipCambioPPal;
                        }

                    }

                    break;

                case (int)Servidor.RolEquipo.SECUNDARIO:
                    ipControl = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorSecundario];
                    ipControlOtro = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServidorPrincipal];
                    ipServiciosSupervisados = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosSecundario];
                    if (NetworkManagement.ipConfigurada(ipServiciosSupervisados))
                    {
                        ipConf = ipServiciosSupervisados;
                    }
                    else
                    {
                        String ipSecundarioComoPpal = (String)GestorParametrosIniciales.Instance.getParametros()[GestorParametrosIniciales.TEXTO_ipServiciosSupervisadosPrincipal];
                        if (NetworkManagement.ipConfigurada(ipSecundarioComoPpal))
                        {
                            ipConf = ipSecundarioComoPpal;
                        }

                    }
                    break;
            }

            if (ipControl != null && ipConf != null)
            {
                if (cambiaIp)
                    Thread.Sleep(10000);

                //Si hacemos ping a la ip de control pero no a la de servicios, inhibimos y habilitamos el interfaz de red para recuperarlo siempre que
                //no haya otro pc con esa ip.
                if (hacer_ping(ipControl) && !hacer_ping(ipConf))
                {
                    log.Debug("Activamos desactivamos interfaz de red por ping a ip de control pero no a la de servicios.");
                    NetworkManagement.disableEnableNetworkAdapter(ipConf);

                    if (cambiaIp)
                        GestorParametrosIniciales.Instance.servidorPrincipal.deten_servicios_locales();

                    //if (cambiaIp && ipCambio != null)
                    //{
                    //    Util.CambiarIP(ipConf, ipCambio);

                        //}

                }
                else
                {
                    Thread.Sleep(4000);
                }

            }
            else
            {
                log.Error("Error en configuración, el equipo no tienen ninguna de las ip esperadas.");
            }


        }
        public static void reseteaPc()
        {
            Process.Start("shutdown","/r /t 0");
        }

        public static Boolean SetComputerName(String Name)
        {
            if (Name == null)
                return false;

            String RegLocComputerName = @"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName";
            try
            {
                string compPath = "Win32_ComputerSystem.Name='" + System.Environment.MachineName + "'";
                using (ManagementObject mo = new ManagementObject(new ManagementPath(compPath)))
                {
                    ManagementBaseObject inputArgs = mo.GetMethodParameters("Rename");
                    inputArgs["Name"] = Name;
                    ManagementBaseObject output = mo.InvokeMethod("Rename", inputArgs, null);
                    uint retValue = (uint)Convert.ChangeType(output.Properties["ReturnValue"].Value, typeof(uint));
                    if (retValue != 0)
                    {
                        throw new Exception("Computer could not be changed due to unknown reason.");
                    }
                }

                RegistryKey ComputerName = Registry.LocalMachine.OpenSubKey(RegLocComputerName);
                if (ComputerName == null)
                {
                    throw new Exception("Registry location '" + RegLocComputerName + "' is not readable.");
                }
                if (((String)ComputerName.GetValue("ComputerName")) != Name)
                {
                    throw new Exception("The computer name was set by WMI but was not updated in the registry location: '" + RegLocComputerName + "'");
                }
                ComputerName.Close();
                ComputerName.Dispose();
            }
            catch (Exception ex)
            {
                log.Error("Error no controlado en cambio de nombre de equipo. Nombre<" + Name + ">",ex);
                return false;
            }
            return true;
        }

        public static Boolean cambia_ip_nombre_resetea(String ipActual,String ipNueva,String nombreNuevo)
        {
            Boolean res = true;
            try
            {
                res = CambiarIP(ipActual, ipNueva);
                if (res)
                {
                    res = SetComputerName(nombreNuevo);
                    if (!res)
                        CambiarIP(ipNueva, ipActual);
                }
                if (res)
                    reseteaPc();
            }
            catch (Exception e)
            {
                log.Error("Error no controlado en cambio de ip, nombre y reseteo de equipo .", e);
                res = false;
            }
            return res;
        }

    }

}

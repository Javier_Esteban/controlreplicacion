﻿#copy \\192.168.2.139\Mega2\surveillance.bak  C:\Mega2
#Copy-Item -Path \\172.31.0.15\Mega2\surveillance.bak -Destination C:\Mega2
copy C:\MEGA2\surveillance.bak "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Backup\"
Import-module SQLPS
Invoke-Sqlcmd -ServerInstance MNS01PRINCESA -Query "ALTER DATABASE surveillance SET SINGLE_USER WITH ROLLBACK IMMEDIATE"
Invoke-Sqlcmd -ServerInstance MNS01PRINCESA -Query "RESTORE DATABASE surveillance FROM DISK='SURVEILLANCE.BAK' WITH REPLACE"
Invoke-Sqlcmd -ServerInstance MNS01PRINCESA -Query "ALTER DATABASE surveillance SET MULTI_USER WITH ROLLBACK IMMEDIATE"
